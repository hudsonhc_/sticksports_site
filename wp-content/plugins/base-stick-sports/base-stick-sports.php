<?php

/**
 * Plugin Name: Base Stick Sports
 * Description: Controle base do tema Stick Sports
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


function baseStickSports () {

		// TIPOS DE CONTEÚDO
	conteudosStickSports();

	taxonomiaStickSports();

	metaboxesStickSports();
}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosStickSports (){

		// TIPOS DE CONTEÚDO
		// tipoDestaque();

		tipoTeam();
		tipoOurSolutions();
		tipoperguntas();
		
		


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
				$titulo = 'Título do destaque';
				break;

				case 'team':
				$titulo = 'Enter member name here';
				break;
				
				default:
				break;
			}

			return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
			'name'               => 'Destaques',
			'singular_name'      => 'destaque',
			'menu_name'          => 'Destaques',
			'name_admin_bar'     => 'Destaques',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaque 	= array(
			'labels'             => $rotulosDestaque,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-megaphone',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaque' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}
	function tipoTeam() {

		$rotulosTeam = array(
			'name'               => 'Stick Sports team',
			'singular_name'      => 'Stick Sports team',
			'menu_name'          => 'Stick Sports team',
			'name_admin_bar'     => 'Stick Sports team',
			'add_new'            => 'Add new',
			'add_new_item'       => 'Add new member of the team',
			'new_item'           => 'New member of the team',
			'edit_item'          => 'Edit member of the team',
			'view_item'          => 'Ver member of the team',
			'all_items'          => 'All team members',
			'search_items'       => 'Search member of the team',
			'parent_item_colon'  => 'From team members',
			'not_found'          => 'No member registered.',
			'not_found_in_trash' => 'No member in the bin.'
		);

		$argsTeam 	= array(
			'labels'             => $rotulosTeam,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 2,
			'menu_icon'          => 'dashicons-groups',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'team' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('team', $argsTeam);

	}
	function tipoOurSolutions() {

		$rotulosSolutions = array(
			'name'               => 'Our Solutions',
			'singular_name'      => 'Our Solution',
			'menu_name'          => 'Our Solutions',
			'name_admin_bar'     => 'Our Solutions',
			'add_new'            => 'Add new',
			'add_new_item'       => 'Add new item',
			'new_item'           => 'New item',
			'edit_item'          => 'Edit item',
			'view_item'          => 'Ver item',
			'all_items'          => 'All items',
			'search_items'       => 'Search items',
			'parent_item_colon'  => 'From items',
			'not_found'          => 'No item registered.',
			'not_found_in_trash' => 'No item in the bin.'
		);

		$argsSolutions 	= array(
			'labels'             => $rotulosSolutions,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 2,
			'menu_icon'          => 'dashicons-yes',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'solutions' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('solutions', $argsSolutions);

	}
	function tipoperguntas() {

		$rotulosFrequentlyaskedQuestions = array(
			'name'               => 'frequently asked questions',
			'singular_name'      => 'frequently asked questions',
			'menu_name'          => 'Frequently asked questions',
			'name_admin_bar'     => 'frequently asked questions',
			'add_new'            => 'Add new',
			'add_new_item'       => 'Add new item',
			'new_item'           => 'New item',
			'edit_item'          => 'Edit item',
			'view_item'          => 'Ver item',
			'all_items'          => 'All items',
			'search_items'       => 'Search items',
			'parent_item_colon'  => 'From items',
			'not_found'          => 'No item registered.',
			'not_found_in_trash' => 'No item in the bin.'
		);

		$argsFrequentlyaskedQuestions 	= array(
			'labels'             => $rotulosFrequentlyaskedQuestions,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 2,
			'menu_icon'          => 'dashicons-format-status',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'frequently-asked-questions'),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('questions', $argsFrequentlyaskedQuestions);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaStickSports () {		
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
	function taxonomiaCategoriaDestaque() {

		$rotulosCategoriaDestaque = array(
			'name'              => 'Categorias de destaque',
			'singular_name'     => 'Categoria de destaque',
			'search_items'      => 'Buscar categorias de destaque',
			'all_items'         => 'Todas as categorias de destaque',
			'parent_item'       => 'Categoria de destaque pai',
			'parent_item_colon' => 'Categoria de destaque pai:',
			'edit_item'         => 'Editar categoria de destaque',
			'update_item'       => 'Atualizar categoria de destaque',
			'add_new_item'      => 'Nova categoria de destaque',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de destaque',
		);

		$argsCategoriaDestaque 		= array(
			'hierarchical'      => true,
			'labels'            => $rotulosCategoriaDestaque,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categoria-destaque' ),
		);

		register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

	}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesStickSports(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

		$prefix = 'StickSports_';

			// METABOX DE DESTAQUE
		$metaboxes[] = array(

			'id'			=> 'detailsMetaboxTeam',
			'title'			=> 'Team member details',
			'pages' 		=> array( 'team' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Member position: ',
					'id'    => "{$prefix}member_position",
					'desc'  => 'Enter here the member position in the team.',
					'type'  => 'text',
				),
				array(
					'name'  => 'Linkedin url: ',
					'id'    => "{$prefix}member_linkedin",
					'desc'  => 'Enter here the linkedin url.',
					'type'  => 'text',
				),
			),
		);

		return $metaboxes;
	}

	function metaboxjs(){

		global $post;
		$template = get_post_meta($post->ID, '_wp_page_template', true);
		$template = explode('/', $template);
		$template = explode('.', $template[1]);
		$template = $template[0];

		if($template != ''){
			wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		}
	}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesStickSports(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerStickSports(){

		if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseStickSports');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

		baseStickSports();

		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );