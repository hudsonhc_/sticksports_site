<?php
/**
 * Template Name: Home
 * Description: Home
 *
 * @package crisecia
 */
get_header(); ?>
<!-- HOM PAGE -->
<div class="pg homePageOne">
	
	<section class="highlights">
		<h6 class="hidden"><?php echo $configuracao['header_logo'] ?></h6>

		<div class="highlightsText">
			<h2><?php echo $configuracao['initial_title'] ?></h2>
			<p class="deskTop"><?php echo $configuracao['initial_description'] ?></p>
		</div>

		<div class="btnHighlights">
			<div class="row">
				<div class="col-sm-6">
					<div class="btns">
						<?php 
							if ($configuracao['initial_link_check'] == "1") {
								$poUpModal = "poUpModal";
							}
						?>
						<a href="<?php echo $configuracao['initial_link_button'] ?>" id="<?php echo $poUpModal ?>"><?php echo $configuracao['initial_text_button_join_our_whitelist'] ?></a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="btns">
						<h2><span>#</span>Bring<span>Your</span>Own</span>Bat<span></h2>
					</div>
				</div>
			</div>
		</div>

		<div class="highlightsText ">
			<p class="mobile"><?php echo $configuracao['initial_description'] ?></p>
		</div>

	</section>

	<section class="information">
		<div class="informationNumber">
			<div class="row">
				<div class="col-sm-6">
					<div class="info">
						<?php 

							$frase  =  explode("|", $configuracao['community_title']);

						?>
						<h2><?php echo $frase[0] ?> <br><b><?php echo $frase[1] ?></b></h2>
					</div>
				</div>		
				<div class="col-sm-6">
					<div class="info">
						<div>
							<strong><?php echo $configuracao['community_top_value']; ?> <?php echo $configuracao['community_top_description']; ?></strong>
						</div>
						<div>
							<strong><?php echo $configuracao['community_middle_value']; ?> <?php echo $configuracao['community_middle_description']; ?></strong>
						</div>
						<div>
							<strong><?php echo $configuracao['community_bottom_value']; ?> <?php echo $configuracao['community_bottom_description']; ?></strong>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</section>

</div>

<div class="pg videoInfo">
	<div class="skewhorizon">
		<div class="video" id="videoOpen">
			<video  class="video0" style="background:url(<?php echo $configuracao['video_foto_url']['url'] ?>)" id="video0">
				<source src="<?php echo $configuracao['video_url'] ?>" type="video/mp4">
			</video>
		</div>
		<div class="info">
			<h2><?php echo $configuracao['video_title']; ?></h2>
			<strong><?php echo $configuracao['video_subtitle']; ?></strong>
			<p><?php echo $configuracao['video_description']; ?></p>
		</div>
	</div>
</div>

<div class="pg ourSolutions">
	
	<section class="ourSolutionsText correctionSkew">
		
		<div class="containerFull">
			<div class="title" id="solution">
				<?php 

					$frases  =  explode("|", $configuracao['our_solution_title']);

				?>
				<h2><?php echo $frases[0] ?> <strong><?php echo $frases[1] ?></strong></h2>
				<p><?php echo $configuracao['our_solution_description']; ?></p>
			</div>
		</div>

		<div class="solutionsList">
			<ul>
				<?php
	                $teamPost = new WP_Query( array( 'post_type' => 'solutions', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -3 ) );
	                while ( $teamPost->have_posts() ) : $teamPost->the_post();

	                $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                $foto = $foto[0];
	         
	            ?>
				<li>
					<figure>
						<img src="<?php echo $foto ?>">
					</figure>
					<h2><?php echo get_the_title() ?></h2>
					<p><?php echo get_the_content() ?> </p>
				</li>
			<?php endwhile; wp_reset_query() ?>
			</ul>
		</div>

	</section>
	<?php 

		$our_downloads_title  =  explode("|", $configuracao['our_downloads_title']);
		$our_downloads_caption  =  explode("|", $configuracao['our_downloads_caption']);
		$our_downloads_description  =   $configuracao['our_downloads_description'];

	?>
	<section class="downloads correctionSkew">
		<div class="area">
			<img src="<?php echo get_template_directory_uri(); ?>/img/cloud-download.png">
			<h4><?php echo $our_downloads_title[0]  ?> <strong><?php echo $our_downloads_title[1]  ?></strong></h4>
			<p> <strong><?php echo $our_downloads_caption [0] ?></strong><?php echo $our_downloads_caption [1] ?></p>
			<span><?php echo $our_downloads_description  ?> </span>
		</div>
	</section>

	<section class="newsllater correctionSkew">
		<img src="<?php echo get_template_directory_uri(); ?>/img/envelope.png">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-4">
					<div class="title">	
						<h6>Sign up to learn more </h6>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="form">
						<?php  echo get_sidebar(); ?>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<?php 

		$live_feed_title  =  explode("|", $configuracao['live_feed_title']);
		$about_title  =  explode("|", $configuracao['about_title']);
		
	?>
	<section class="liveFeed correctionSkew">
		<div class="containerFull">
			<h6><?php echo $live_feed_title[0] ?> <strong><?php echo $live_feed_title[1] ?></strong></h6>
		</div>

		<div class="areaCarrossel">
			<div id="carrosselDepoimentos" class="carrosselDepoimentos"></div>
	</section>

	<section class="aboutStickSports correctionSkew">
		
		<div class="titulo">
			<h2><?php echo $about_title[0] ?> <strong><?php echo $about_title[1] ?></strong></h2>
			<p><?php echo $configuracao['about_description'] ?></p>
		</div>

		<ul class="countNumber">
			<li class="numberOne">
				<span class="number" data-stop="<?php echo $configuracao['about_value_left'] ?>">0</span>
				<h4><?php echo $configuracao['about_description_left'] ?>s</h4>
			</li>
			<li class="numberTwo">
				<span class="number" data-stop="<?php echo $configuracao['about_value_center'] ?>">0</span>
				<h4><?php echo $configuracao['about_description_center'] ?></h4>
			</li>
			<li class="three">
				<span class="number" data-stop="<?php echo $configuracao['about_value_right'] ?>">0</span>
				<h4><?php echo $configuracao['about_description_right'] ?></h4>
			</li>
		</ul>
	</section>
	<?php 

		$team_title  =  explode("|", $configuracao['team_title']);
		
		
	?>
	<div class="containerFull">
		<div class="titlePage correctionSkew">
			<h2><?php echo $team_title[0] ?><strong><?php echo $team_title[1] ?></strong></h2>
		</div>
	</div>

</div>

<div class="meettheTeam">
	<div class="correctionSkew">
		<div class="row">
			<div class="col-md-6">
				<div class="figureImg" id="team">
					<?php
					 	$k = 0; 
		                $teamPost = new WP_Query( array( 'post_type' => 'team', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
		                while ( $teamPost->have_posts() ) : $teamPost->the_post();

		                $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		                $foto = $foto[0];

		            	$member_position = rwmb_meta( 'StickSports_member_position');
		            	$member_linkedin = rwmb_meta( 'StickSports_member_linkedin');
		            	if ( $k == 0):
		            		
		            	
		            ?>
					<figure>
						<a href="<?php 	echo $member_linkedin ?>" id="linkedin" target="_blank">
							<img id="imgTeam" src="<?php echo $foto  ?>" alt="<?php echo get_the_title() ?>">
						</a>
					</figure>
				<?php endif;$k++;endwhile; wp_reset_query() ?>
				</div>
			</div>
			<div class="col-md-6">
				 <?php
				 	$j = 0; 
	                $teamPost = new WP_Query( array( 'post_type' => 'team', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	                while ( $teamPost->have_posts() ) : $teamPost->the_post();

	                $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	                $foto = $foto[0];
	                $member_linkedin = rwmb_meta( 'StickSports_member_linkedin');
	            	$member_position = rwmb_meta( 'StickSports_member_position' );
	            	if ( $j == 0):
	            		
	            	
	            ?>
				<div class="team deskop">
					<h2 class="teamName"><?php echo get_the_title() ?></h2>
					<span class="teamFunction"><?php echo $member_position ?></span>

					<p class="teamDescription"><?php echo get_the_content(); ?></p>
				</div>
				<?php endif;$j++;endwhile; wp_reset_query() ?>
				
				<div class="carrosselTeam" id="carrosselTeam">
					 <?php
					 	$i = 0; 
	                    $teamPost = new WP_Query( array( 'post_type' => 'team', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	                    while ( $teamPost->have_posts() ) : $teamPost->the_post();
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
	                			$member_position = rwmb_meta( 'StickSports_member_position' );
	                			$member_linkedinUrl = rwmb_meta( 'StickSports_member_linkedin');
	                	if ( $i == 0) {
	                		$ativo = "active";
	                	}
	                ?>
					<div class="item <?php echo $ativo ?>" dataLinkdin="<?php echo $member_linkedinUrl ?>"  dataName="<?php echo get_the_title() ?>" dataFunction="<?php echo $member_position ?>" dataDescription="<?php echo get_the_content() ?>" dataUrl="<?php echo $foto ?>">
						<figure style="background:url(<?php echo $foto ?>)">
							<img src="<?php echo $foto ?>" alt="<?php echo get_the_title() ?>">
						</figure>
					</div>
				  <?php $i++;endwhile; wp_reset_query() ?>
				</div>
				<div class="buttonCarrossel">
					<button class="btnLeft"></button>
					<button class="btnlRight"></button>
				</div>
				 <?php
				 	$k = 0; 
	                $teamPost = new WP_Query( array( 'post_type' => 'team', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
	                while ( $teamPost->have_posts() ) : $teamPost->the_post();

	            	$member_position = rwmb_meta( 'StickSports_member_position' );
	            	if ( $k == 0):
	            		
	            	
	            ?>
				<div class="team mobile">
					<h2 class="teamName"><?php echo get_the_title() ?></h2>
					<span class="teamFunction"><?php echo $member_position ?></span>

					<p class="teamDescription"><?php echo get_the_content(); ?></p>
				</div>
				<?php endif;$k++;endwhile; wp_reset_query() ?>

			</div>
		</div>
	</div>
	
</div>

<?php 
	$live_players_title  =  explode("|", $configuracao['live_players_title']);
	$live_players_time  =  explode("|", $configuracao['live_players_time']);
	$live_players_desc  =   $configuracao['live_players_desc'];	
?>
<section class="mapa">
	<div class="correctionSkew">

		<div class="caixa">
			<div class="containerFull">
				<h2> <?php echo $live_players_title[0] ?> <strong><?php echo $live_players_title[1] ?></strong></h2>  
				<div class="row">
					<div class="col-md-5">
						<div class="count"></div>
					</div>  
					<div class="col-md-7">
						<div class="info">
							<strong> <?php  echo $live_players_desc  ?></strong>
							<p> <?php echo $live_players_time[0]  ?><span><?php echo $live_players_time[1]  ?></span></p>    
						</div>
					</div>
				</div>
			</div>
	    </div>
	    <?php 

			$own_the_game_title  =  explode("|", $configuracao['own_the_game_title']);
			
		?>
		<div class="titlePage">
			<h4><?php echo $own_the_game_title[0]  ?></h4>
			<h2><?php echo $own_the_game_title[1]  ?></h2>
			<a href="<?php echo $configuracao['own_the_game_link_button'] ?>"><?php echo $configuracao['own_the_game_button_text'] ?></a>
		</div>

	</div>
</section>
<?php get_footer(); ?>