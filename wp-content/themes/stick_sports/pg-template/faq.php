<?php
/**
 * Template Name: FAQ
 * Description: FAQ
 *
 * @package crisecia
 */
get_header(); ?>
	<div class="faq">
		
	<section class="newsllater">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-12">
					<div class="title">	
						<h6>How can we help you?</h6>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form">
						<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
						<input type="text" name="s" id="search" placeholder="Search help articles">
						<input type="Submit" value="Search">
						</form>
					</div>
				</div>
				
			</div>
		</div>
	</section>
		
		<section class="frequently">
			<div class="containerFull">
				<div class="row">

					<div class="col-sm-6">
						<div class="infoTitle">
							<h2>Frequently <br> Asked Questions</h2>

							<div class="contact">
								<span>Contact us anytime</span>
								<a href="">enquiries@sticksports.com</a>
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<?php 
							$questions =  new WP_Query( array('post_type' => 'questions'));

							while($questions ->have_posts()): $questions->the_post();
						?>
						<div class="questions">
							<button class="question" data-collapse="collapseOne"><span></span><?php echo get_the_title() ?></button>
							<div class="description" id="collapseOne">
								<h2><?php echo get_the_title() ?></h2>
								<?php echo the_content(); ?> 
							</div>
						</div>

					<?php endwhile;wp_reset_query(); ?>

					</div>

				</div>
			</section>
		</div>

		</div>
		
	
<?php get_footer(); ?>