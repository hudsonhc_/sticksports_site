<?php
//error_reporting(E_ALL & ~E_NOTICE);
//ini_set('display_errors', '1');

require_once ('web-games/global/SendyPHP.php');

$sendyAPI = "wB8t7rxxZ1EtaCud2VD4";
$sendyInstall = "http://sendy.sticksports.com";
$sendyList = 'nDQNsNst93Br8929irItuFhA';

$email = $_GET['subscribe'];

// Remove all illegal characters from email
$emailClean = filter_var($email, FILTER_SANITIZE_EMAIL);

$geoCode = strtoupper($_SERVER["HTTP_CF_IPCOUNTRY"]);
$referrer = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$ipaddress = $_SERVER['REMOTE_ADDR'];

// Validate e-mail
if (filter_var($emailClean, FILTER_VALIDATE_EMAIL)) {
    $config = array(
        'api_key' => $sendyAPI, //your API key is available in Settings
        'installation_url' => $sendyInstall,  //Your Sendy installation
        'list_id' => $sendyList
    );

    $sendy = new SendyPHP($config);
    $results = $sendy->subscribe(array(
                'name'=>'',
                'email' => $emailClean, //this is the only field required by sendy
                'country' => $geoCode,
                'ipaddress' => $ipaddress,
                'referrer' => $referrer,
                'Country' => strtolower($geoCode),
                'Source' => "bc-subscribe"
                ));

    header("Location: /bc-subscribe/");
    exit;
}

//Else something has gone wrong, so show failed
header("Location: /bc-subscribe-failed/");
exit;
?>
