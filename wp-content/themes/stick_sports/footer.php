<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stick_Sports
 */
global $configuracao;
?>
<footer class="rodape">
	<div class="containerFull">
		<hr>

		<div class="row logoNav">
			<div class="col-sm-5">
				<div class="logo">
					<img src="<?php echo $configuracao['footer_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
				</div>
			</div>
			<div class="col-sm-7">
				<nav>
				<?php 
					$menu = array(
						'theme_location'  => '',
						'menu'            => 'Stick Sports',
						'container'       => false,
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 2,
						'walker'          => ''
						);
					wp_nav_menu( $menu );
				?>
				</nav>
			</div>
		</div>
		<?php 

			$footer_contact_mail  =  explode("@", $configuracao['footer_contact_mail']);
			
			$footer_head_office  =   $configuracao['footer_head_office'];
			$footer_copyright  =   $configuracao['footer_copyright'];

		?>
		<div class="row infoContact">
			<div class="col-sm-6">
				<div class="emailContact">
					<a href="malito:<?php echo $footer_contact_mail[0] ?>@<?php echo $footer_contact_mail[1] ?>"><?php echo $footer_contact_mail[0] ?><span>@</span><?php echo $footer_contact_mail[1] ?></a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="contactRedesSociais">
                 <?php if ($configuracao['social_facebook']): ?>
                    <a href="<?php  echo $configuracao['social_facebook'] ?>" target="_blank" title="Facebook" class="facebook">Facebook <img alt="Facebook" src="<?php echo get_template_directory_uri(); ?>/img/f.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_instagram']): ?>
                    <a href="<?php  echo $configuracao['social_instagram'] ?>" target="_blank" title="Instagram" class="instagram">Instagram <img alt="instagram" src="<?php echo get_template_directory_uri(); ?>/img/in.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_twitter']): ?>
                    <a href="<?php  echo $configuracao['social_twitter'] ?>" target="_blank" title="Twitter" class="twitter">Twitter <img alt="Twitter" src="<?php echo get_template_directory_uri(); ?>/img/t.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_medium']): ?>
                    <a href="<?php     echo $configuracao['social_medium'] ?>" target="_blank" title="Medium" class="medium">Medium <img alt="Medium" src="<?php echo get_template_directory_uri(); ?>/img/m.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_linkedin']): ?>
                    <a href="<?php  echo $configuracao['social_linkedin'] ?>" target="_blank" title="Linkedin" class="linkedin">Linkedin <img alt="Linkedin" src="<?php echo get_template_directory_uri(); ?>/img/i.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_bitcoin']): ?>
                    <a href="<?php  echo $configuracao['social_bitcoin'] ?>" target="_blank" title="Bitcoin" class="bitcoin">Bitcoin <img alt="Bitcoin" src="<?php echo get_template_directory_uri(); ?>/img/b.png"></a>
                    <?php
                        endif;
                            if ($configuracao['social_reddit']): ?>
                    <a href="<?php  echo $configuracao['social_reddit'] ?>" target="_blank" title="Reddit" class="reddit">Reddit <img alt="Reddit" src="<?php echo get_template_directory_uri(); ?>/img/red.png"></a>
                    <?php endif;
                            if ($configuracao['social_telegram']): ?>
                    <a href="<?php  echo $configuracao['social_telegram'] ?>" target="_blank" title="Telegram" class="reddit">Telegram <img alt="Telegram" src="<?php echo get_template_directory_uri(); ?>/img/telegram.png"></a>
                    <?php endif; ?>
				</div>
			</div>
		</div>

		<hr>

		<div class="row infocopyRight">
			<div class="col-sm-6">
				<div class="copyRight">
					<p><?php echo $footer_head_office  ?> </p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="copyRight right">
					<p><?php echo $footer_copyright ?></p>
				</div>
			</div>
		</div>

	</div>
</footer>
<?php
    wp_footer();
    
?>

</body>
</html>

<script type="text/javascript">
		
		const runsURL = "https://us-central1-statsproject-192316.cloudfunctions.net/GetLiveRunsData";
		const reviewsURL = "https://us-central1-statsproject-192316.cloudfunctions.net/GetLiveReviews";
        var reviews;
        var scrollSize = 0;
        var count = d3.select(".count");
        var scrollSVG = d3.select(".viewport").append("svg").attr("class", "scroll-svg");
        var defs = scrollSVG.insert("defs", ":first-child");
        var chartGroup = scrollSVG.append("g").attr("class", "chartGroup");
        var width = 1900, height = 850;   
        var π = Math.PI,
        radians = π / 180,
        degrees = 180 / π, 
        circle = d3.geoCircle()
                    .precision(90),
        mapProj,
        geoPath,
        map,
        night,
        sessions,
        sessionsData,
        timeout = 0,
        tickTimeout,
        worldGraph = d3.select('.caixa').append('svg').attr("width", width).attr("height",height).attr("class", "svgLargura")


//**********************
		//COMENTARIOS 
//***********************//
	getReviews();
	setTimeout(function(){
		//getReviewStar();
	}, 4000);
   
    function getReviews(){
        d3.json(reviewsURL, function (data){   
            reviews = data;
            	creatLayoutReviews(reviews);
        });
    }

    function creatLayoutReviews(reviews){
     	reviews = reviews;
      for(let i = 0; i < reviews.length;i++){
            let stars = reviews[i]["score"];
            let systemIcon
            let data = 123;
            let avaliacao; 
            if (reviews[i]["System"] == "GooglePlay"){
            	systemIcon = "GooglePlay";
            }else{
            	systemIcon = "appleIcon";
            }
           
            if (stars == 5) {
                avaliacao = "<span></span><span></span><span></span><span></span><span></span>";
            }else if(stars == 4){
                 avaliacao = "<span></span><span></span><span></span><span></span><span class='ativo'></span>";
            }
            else if(stars == 3){
                avaliacao = "<span></span><span></span><span></span><span class='ativo'></span><span class='ativo'></span>";
            }
            else if(stars == 2){
                avaliacao = "<span></span><span></span><span class='ativo'></span><span class='ativo'></span><span class='ativo'></span>";
            }
            else if(stars == 1){
                 avaliacao = "<span></span><span class='ativo'></span><span class='ativo'></span><span class='ativo'></span><span class='ativo'></span>";
            }else{
                 avaliacao = "<span class='ativo'></span><span class='ativo'></span><span class='ativo'></span><span class='ativo'></span><span class='ativo'></span>";
            }
			
            $(".carrosselDepoimentos").append("<div class='item' data='"+stars+"'><div class='comments'><div class='row'><div class='col-sm-12'><figure class='"+systemIcon+"' ></figure></div><div class='col-sm-12'><div class='info'><strong>"+data+"</strong><h2>"+reviews[i]["userName"]+"</h2><div id='stars"+stars+"' class='stars' dataStars='"+stars+"'>"+avaliacao+"</div><p>"+reviews[i]["text"]+"</p></div></div></div></div></div>");
        }
    }

    function getReviewStar(){
    	
    	$(".carrosselDepoimentos .item").each(function(){
       		let qtdStars = $(this).attr("data");
       		// console.log(qtdStars);
            $("#stars"+qtdStars).append("<span></span>");
       		// if (qtdStars == 1) {
       		// 	$("#stars"+qtdStars).append("<span></span>");
       		// }else if (qtdStars == 2) {
       		// 	$("#stars"+qtdStars).append("<span></span>");
       		// }else if (qtdStars == 3) {
       		// 	$("#stars"+qtdStars).append("<span></span>");
       		// }else if (qtdStars == 4) {
       		// 	$("#stars"+qtdStars).append("<span></span>");
       		// }else if (qtdStars == 5) {
       		// 	$("#stars"+qtdStars).append("<span></span>");
       		// }
    	});


    }


//**********************
		//MAPA 
//***********************//
                    

    // radial progression
    var radialGradient = worldGraph.append("defs")
            .append("radialGradient")
            .attr("id", "radial-gradient");

        radialGradient.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", "#D70000");

        radialGradient.append("stop")
            .attr("offset", "100%")
            .attr("stop-color", "#d7000087");

    Tick();

    function Tick()
    {
        requestRuns();
        chartGroup.append("rect")
            .attr("fill", "#FFFFFF");
        mapProj = createMapProjection();
        geoPath = d3.geoPath().projection(mapProj);
        map = worldGraph.append('g').attr("id","map");
        night = worldGraph.insert("path",".night");
        sessions = worldGraph.append("g").attr("id","sessions");
        makeLiveDataRequest(); 
        mapDrawLoop();

    }

    function mapDrawLoop()
    { 
        if(!tickTimeout)
        {
            tickTimeout = setInterval(mapDrawLoop,15000);//TODO: change to 60000
           
        }else
        { 
            //TODO: destroy and create map circles this could be improved with an object pool or someway to update the map 
            //TODO: write doc 
            //mapProj.rotate([1000/1000-40]);
           // geoPath = d3.geoPath().projection(mapProj);
           // drawMap(mapProj,worldData); 
        }
        //console.log("drawing map");
        drawMap(worldData);  
        drawNight();
    }

    function createMapProjection()
    {
        var projection = d3.geoEquirectangular() //geoOrthographic geoEquirectangular
                                    .scale((width- 2) / (2 * Math.PI)) //TODO: fixed width
                                    .rotate([-0.4, 0])
                                    .center([0, 0])
                                    .translate([width / 2.8, height / 3]) //TODO: fixed width
                                    .precision(.1);
        return projection;
    }
    
    function drawMap(data)
    {
        map.selectAll('path')
        .data(data.features)
        .enter()
        .append('path')
        .attr('d', geoPath)
        .on('click',function(d){
            console.log(d);
            return
        });
    }

    function drawNight()
    {
        night.datum(circle.center(antipode(solarPosition(new Date))))
        .attr("class", "night")
        .attr("d", geoPath);
    }

    function antipode(position) {
        return [position[0] + 180, -position[1]];
    }

    function solarPosition(time) {
        var centuries = (time - Date.UTC(2000, 0, 1, 12)) / 864e5 / 36525, // since J2000
            longitude = (d3.timeDay.floor(time) - time) / 864e5 * 360 - 180;
            return [
                longitude - equationOfTime(centuries) * degrees,
                solarDeclination(centuries) * degrees
            ];
    }

    function equationOfTime(centuries) {
        var e = eccentricityEarthOrbit(centuries),
        m = solarGeometricMeanAnomaly(centuries),
        l = solarGeometricMeanLongitude(centuries),
        y = Math.tan(obliquityCorrection(centuries) / 2);
        y *= y;
        return y * Math.sin(2 * l)
        - 2 * e * Math.sin(m)
        + 4 * e * y * Math.sin(m) * Math.cos(2 * l)
        - 0.5 * y * y * Math.sin(4 * l)
        - 1.25 * e * e * Math.sin(2 * m);
    }

    function solarDeclination(centuries) {
        return Math.asin(Math.sin(obliquityCorrection(centuries)) * Math.sin(solarApparentLongitude(centuries)));
    }

    function solarApparentLongitude(centuries) {
        return solarTrueLongitude(centuries) - (0.00569 + 0.00478 * Math.sin((125.04 - 1934.136 * centuries) * radians)) * radians;
    }

    function solarTrueLongitude(centuries) {
        return solarGeometricMeanLongitude(centuries) + solarEquationOfCenter(centuries);
    }

    function solarGeometricMeanAnomaly(centuries) {
        return (357.52911 + centuries * (35999.05029 - 0.0001537 * centuries)) * radians;
    }

    function solarGeometricMeanLongitude(centuries) {
        var l = (280.46646 + centuries * (36000.76983 + centuries * 0.0003032)) % 360;
        return (l < 0 ? l + 360 : l) / 180 * π;
    }

    function solarEquationOfCenter(centuries) {
        var m = solarGeometricMeanAnomaly(centuries);
        return (Math.sin(m) * (1.914602 - centuries * (0.004817 + 0.000014 * centuries))
        + Math.sin(m + m) * (0.019993 - 0.000101 * centuries)
        + Math.sin(m + m + m) * 0.000289) * radians;
    }

    function obliquityCorrection(centuries) {
        return meanObliquityOfEcliptic(centuries) + 0.00256 * Math.cos((125.04 - 1934.136 * centuries) * radians) * radians;
    }

    function meanObliquityOfEcliptic(centuries) {
        return (23 + (26 + (21.448 - centuries * (46.8150 + centuries * (0.00059 - centuries * 0.001813))) / 60) / 60) * radians;
    }

    function eccentricityEarthOrbit(centuries) {
        return 0.016708634 - centuries * (0.000042037 + 0.0000001267 * centuries);
    }

    function makeLiveDataRequest()
    {
        var Url = "https://us-central1-statsproject-192316.cloudfunctions.net/GetLiveData";
        d3.request(Url,handleResponse)
    }

    function handleResponse(err,body)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            populateMap(JSON.parse(body.response));
            
            if(!timeout)
            {
                timeout = setInterval(makeLiveDataRequest,60000);//TODO: change to 60000
            }
        }
    }

    function populateMap(session_json)
    {

        //;sessions = worldGraph.append("g").selectAll("circle").data(JSON.parse(body.response))

        var sessionsData = sessions.selectAll("circle").data(session_json);
        sessionsData.exit().remove();
        sessionsData.enter()
        .append('circle')
        .attr("transform",function(d)
        { 
            return "translate(" + mapProj([d.coordinates_02,d.coordinates_01])+")";
        }).on('click',function(d){
            console.log(d.countryName,d.sessions,d);
        })
        .style("fill", "url(#radial-gradient)")
        .transition().duration(1000)
        .attr('r',getRadius);

        
        sessionsData.on('click',function(d){
            console.log(d.countryName,d.sessions,d);
        }).attr("transform",function(d)
        { 
            return "translate(" + mapProj([d.coordinates_02,d.coordinates_01])+")";
        }).on('click',function(d){
            console.log(d.countryName,d.sessions,d);
        })
        .style("fill", "url(#radial-gradient)")
        .transition().duration(1000)
        .attr('r',getRadius);
    }

    function getRadius(d)
    {
        let  radius = 5;
            
        if(d.sessions >= 150)
        {
            radius =  15;
        }
        else if(d.sessions >= 50)
        {
            radius = 10;
        }
        //console.log(d);
        return radius;
    }

    function requestRuns()
    {
        d3.json(runsURL, function(body){
            //console.log(body);
            if(body.length > 0)
            {
                count.text(parseInt(body[0].runs).toLocaleString('en'));
            }
            setTimeout(requestRuns, 1000);
        });
    }

        
</script>
