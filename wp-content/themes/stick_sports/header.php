<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stick_Sports
 */
	global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo $configuracao['header_favico']['url'] ?>" type="image/x-icon"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<!-- TOP -->
<header>
	<div class="containerFull">
		<div class="contactRedesSociais socialtopo">
			<?php if ($configuracao['social_facebook']): ?>
		    <a href="<?php  echo $configuracao['social_facebook'] ?>" target="_blank" title="Facebook" class="facebook">Facebook <img alt="Facebook" src="<?php echo get_template_directory_uri(); ?>/img/f.png"></a>
		  	<?php
		  		endif;
					if ($configuracao['social_instagram']): ?>
		    <a href="<?php  echo $configuracao['social_instagram'] ?>" target="_blank" title="Instagram" class="instagram">Instagram <img alt="instagram" src="<?php echo get_template_directory_uri(); ?>/img/in.png"></a>
		    <?php
		    	endif;
					if ($configuracao['social_twitter']): ?>
		    <a href="<?php  echo $configuracao['social_twitter'] ?>" target="_blank" title="Twitter" class="twitter">Twitter <img alt="Twitter" src="<?php echo get_template_directory_uri(); ?>/img/t.png"></a>
			<?php
				endif;
					if ($configuracao['social_medium']): ?>
			<a href="<?php     echo $configuracao['social_medium'] ?>" target="_blank" title="Medium" class="medium">Medium <img alt="Medium" src="<?php echo get_template_directory_uri(); ?>/img/m.png"></a>
		    <?php
		    	endif;
					if ($configuracao['social_linkedin']): ?>
		    <a href="<?php  echo $configuracao['social_linkedin'] ?>" target="_blank" title="Linkedin" class="linkedin">Linkedin <img alt="Linkedin" src="<?php echo get_template_directory_uri(); ?>/img/i.png"></a>
		    <?php
		    	endif;
					if ($configuracao['social_bitcoin']): ?>
		    <a href="<?php  echo $configuracao['social_bitcoin'] ?>" target="_blank" title="Bitcoin" class="bitcoin">Bitcoin <img alt="Bitcoin" src="<?php echo get_template_directory_uri(); ?>/img/b.png"></a>
		    <?php
		    	endif;
					if ($configuracao['social_reddit']): ?>
		    <a href="<?php  echo $configuracao['social_reddit'] ?>" target="_blank" title="Reddit" class="reddit">Reddit <img alt="Reddit" src="<?php echo get_template_directory_uri(); ?>/img/red.png"></a>
			<?php endif;
					if ($configuracao['social_telegram']): ?>
		    <a href="<?php  echo $configuracao['social_telegram'] ?>" target="_blank" title="Telegram" class="reddit">Telegram <img alt="Telegram" src="<?php echo get_template_directory_uri(); ?>/img/telegram.png"></a>
			<?php endif; ?>
		</div>
	
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<div class="logoBrand">
					<a href="<?php echo home_url('/');?>">
						<img src="<?php echo $configuracao['header_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
					</a>
				</div>
			</div>
			<div class="col-xs-1 col-sm-6">
				<nav id="navMenu">
					<span id="closeMenu"></span>
					<?php 
						$menu = array(
							'theme_location'  => '',
							'menu'            => 'Stick Sports',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'menu',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 2,
							'walker'          => ''
							);
						wp_nav_menu( $menu );
					?>
				</nav>
			</div>
			<div class="col-xs-5 col-sm-3">
				
				<button id="openMenu">
					<span></span>
					<span></span>
					<span></span>
				</button>

				<div class="right">
					<span id="modalOpen"><?php echo $configuracao['header_login_button_text'] ?></span>
				</div>

			</div>
		</div>
	</div>
</header>

<div class="modalLogin" id="modalLogin">
	<div class="formModal">
		<span id="modalClose"></span>
		<h2>Sign up for more information</h2>
		<form>
			<?php  echo  dynamic_sidebar( 'SidebarHeader' ); ?>
		</form>
		<p>I agree that my email will be compiled and processed by Stick Sports Ltd for marketing purposes. I can change this statement at any time. I confirm that I am over the age of 18. For more information please read our Privacy policy.</p>
	</div>
</div>	

<div class="modalVideo" id="modalVideo">
	<div class="formModal">
		<span id="modalCloseVideo"></span>
		<video controls  class="video0" style="background:url(<?php echo $configuracao['video_foto_url']['url'] ?>)" id="playVideo">
			<source src="<?php echo $configuracao['video_url'] ?>" type="video/mp4">
		</video>
	</div>
</div>	
