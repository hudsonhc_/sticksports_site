<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
    /*********************************************
                //FUNÇÕES//
    **********************************************/
        if ( ! class_exists( 'Redux' ) ) {
            return;
        }


        // This is your option name where all the Redux data is stored.
        $opt_name = "configuracao";

        // This line is only for altering the demo. Can be easily removed.
        //$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

        /*
         *
         * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
         *
         */

        $sampleHTML = '';
        if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
            Redux_Functions::initWpFilesystem();

            global $wp_filesystem;

            $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
        }

        // Background Patterns Reader
        $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns      = array();

        if ( is_dir( $sample_patterns_path ) ) {

            if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
                $sample_patterns = array();

                while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                    if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                        $name              = explode( '.', $sample_patterns_file );
                        $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                        $sample_patterns[] = array(
                            'alt' => $name,
                            'img' => $sample_patterns_url . $sample_patterns_file
                        );
                    }
                }
            }
        }

        $theme = wp_get_theme(); // For use with some settings. Not necessary.

        $args = array(
            // TYPICAL -> Change these values as you need/desire
            'opt_name'             => $opt_name,
            // This is where your data is stored in the database and also becomes your global variable name.
            'display_name'         => $theme->get( 'Name' ),
            // Name that appears at the top of your panel
            'display_version'      => $theme->get( 'Version' ),
            // Version that appears at the top of your panel
            'menu_type'            => 'menu',
            //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
            'allow_sub_menu'       => true,
            // Show the sections below the admin menu item or not
            'menu_title'           => __( 'Edit site information', 'redux-framework-demo' ),
            'page_title'           => __( 'Edit site information', 'redux-framework-demo' ),
            // You will need to generate a Google API key to use this feature.
            // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
            'google_api_key'       => '',
            // Set it you want google fonts to update weekly. A google_api_key value is required.
            'google_update_weekly' => false,
            // Must be defined to add google fonts to the typography module
            'async_typography'     => true,
            // Use a asynchronous font on the front end or font string
            //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
            'admin_bar'            => true,
            // Show the panel pages on the admin bar
            'admin_bar_icon'       => 'dashicons-portfolio',
            // Choose an icon for the admin bar menu
            'admin_bar_priority'   => 50,
            // Choose an priority for the admin bar menu
            'global_variable'      => '',
            // Set a different name for your global variable other than the opt_name
            'dev_mode'             => false,
            // Show the time the page took to load, etc
            'update_notice'        => true,
            // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
            'customizer'           => true,
            // Enable basic customizer support
            //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
            //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

            // OPTIONAL -> Give you extra features
            'page_priority'        => null,
            // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent'          => 'themes.php',
            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions'     => 'manage_options',
            // Permissions needed to access the options panel.
            'menu_icon'            => '',
            // Specify a custom URL to an icon
            'last_tab'             => '',
            // Force your panel to always open to a specific tab (by id)
            'page_icon'            => 'icon-themes',
            // Icon displayed in the admin panel next to your menu_title
            'page_slug'            => '',
            // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
            'save_defaults'        => true,
            // On load save the defaults to DB before user clicks save or not
            'default_show'         => false,
            // If true, shows the default value next to each field that is not the default value.
            'default_mark'         => '',
            // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export'   => true,
            // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time'       => 60 * MINUTE_IN_SECONDS,
            'output'               => true,
            // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag'           => true,
            // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database'             => '',
            // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'use_cdn'              => true,
            // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

            // HINTS
            'hints'                => array(
                'icon'          => 'el el-question-sign',
                'icon_position' => 'right',
                'icon_color'    => 'lightgray',
                'icon_size'     => 'normal',
                'tip_style'     => array(
                    'color'   => 'red',
                    'shadow'  => true,
                    'rounded' => false,
                    'style'   => '',
                ),
                'tip_position'  => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect'    => array(
                    'show' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'mouseover',
                    ),
                    'hide' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'click mouseleave',
                    ),
                ),
            )
        );

        // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-docs',
            'href'  => 'http://docs.reduxframework.com/',
            'title' => __( 'Documentation', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            //'id'    => 'redux-support',
            'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
            'title' => __( 'Support', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-extensions',
            'href'  => 'reduxframework.com/extensions',
            'title' => __( 'Extensions', 'redux-framework-demo' ),
        );
        // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
        $args['share_icons'][] = array(
            'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
            'title' => 'Visit us on GitHub',
            'icon'  => 'el el-github'
            //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
        );
        $args['share_icons'][] = array(
            'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
            'title' => 'Like us on Facebook',
            'icon'  => 'el el-facebook'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://twitter.com/reduxframework',
            'title' => 'Follow us on Twitter',
            'icon'  => 'el el-twitter'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://www.linkedin.com/company/redux-framework',
            'title' => 'Find us on LinkedIn',
            'icon'  => 'el el-linkedin'
        );

        // Panel Intro text -> before the form
        if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
            if ( ! empty( $args['global_variable'] ) ) {
                $v = $args['global_variable'];
            } else {
                $v = str_replace( '-', '_', $args['opt_name'] );
            }
            $args['intro_text'] = sprintf( __( '', 'redux-framework-demo' ) );
        } else {
            $args['intro_text'] = __( '', 'redux-framework-demo' );
        }

        // Add content after the form.
        $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

        Redux::setArgs( $opt_name, $args );

        /*
         * ---> END ARGUMENTS
         */


        /*
         * ---> START HELP TABS
         */

        $tabs = array(
            array(
                'id'      => 'redux-help-tab-1',
                'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            ),
            array(
                'id'      => 'redux-help-tab-2',
                'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            )
        );
        Redux::setHelpTab( $opt_name, $tabs );

        // Set the help sidebar
        $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
        Redux::setHelpSidebar( $opt_name, $content );


        /*
         * <--- END HELP TABS
         */


        /*
         *
         * ---> START SECTIONS
         *
         */

    /*********************************************
              CAMPOS PERSONALIZADOS
    **********************************************/
        //HEADER SECTION
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Header', 'redux-framework-demo' ),
            'id'               => 'opt_site_settings',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // HEADER
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Header settings', 'redux-framework-demo' ),
                    'id'               => 'header_settings',
                    'subsection'       => true,
                    'desc'             => __( 'Header settings', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'header_logo',
                            'type'     => 'media',
                            'title'    => __( 'Logo', 'redux-framework-demo' ),
                            'desc'    => __( 'Insert here the logo of header', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'header_login_button_text',
                            'type'     => 'text',
                            'title'    => __( 'Text of login button', 'redux-framework-demo' ),
                            'desc'    => __( '(ex: Sign up)', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'header_link_button_login',
                            'type'     => 'text',
                            'title'    => __( 'Login button link', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
						array(
                            'id'       => 'header_favico',
                            'type'     => 'media',
                            'title'    => __( 'Favico', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
						
                    )
                )
            );
        //INITIAL SECTION
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Initial section', 'redux-framework-demo' ),
            'id'               => 'opt_initial_section',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-th-list'
            ) 
        );
            //INITIAL SECTION
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Initial section', 'redux-framework-demo' ),
                    'id'               => 'initial_section',
                    'subsection'       => true,
                    'desc'             => __( 'Initial section settings ', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'initial_title',
                            'type'     => 'text',
                            'title'    => __( 'Title: ', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'initial_description',
                            'type'     => 'text',
                            'title'    => __( 'Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'Text of description', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'initial_text_button_join_our_whitelist',
                            'type'     => 'text',
                            'title'    => __( 'Text button: ', 'redux-framework-demo' ),
                            'desc'    => __( '(Join our whitelist)', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'initial_link_button',
                            'type'     => 'text',
                            'title'    => __( 'Link button: (Join our whitelist)', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                          array(
                            'id'       => 'initial_link_check',
                            'type'     => 'checkbox',
                            'title'    => __( 'Atctive Pop-up Sign up', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        //COMMUNITY SECTION
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Community section', 'redux-framework-demo' ),
            'id'               => 'opt_community_section',
            'desc'             => __( 'Info community', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-group'
            ) 
        );
            // COMMUNITY SECTION TITLE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Title', 'redux-framework-demo' ),
                    'id'               => 'community_title',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'community_title',
                            'type'     => 'text',
                            'title'    => __( 'Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: STICK SPORTS | COMMUNITY', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // COMMUNITY SECTION LEFT ITEM
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Top item', 'redux-framework-demo' ),
                    'id'               => 'community_top_item',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'community_top_value',
                            'type'     => 'text',
                            'title'    => __( 'Value of top item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 450k', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'community_top_description',
                            'type'     => 'text',
                            'title'    => __( 'Description of top item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: DAU', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // COMMUNITY SECTION CENTER ITEM
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Middle item', 'redux-framework-demo' ),
                    'id'               => 'community_middle_item',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'community_middle_value',
                            'type'     => 'text',
                            'title'    => __( 'Value of middle item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 55%', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'community_middle_description',
                            'type'     => 'text',
                            'title'    => __( 'Description of middle item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: DAY ONE RETENTION RATE', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // COMMUNITY SECTION CENTER ITEM
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Bottom item', 'redux-framework-demo' ),
                    'id'               => 'community_bottom_item',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'community_bottom_value',
                            'type'     => 'text',
                            'title'    => __( 'Value of bottom item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 800K+', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'community_bottom_description',
                            'type'     => 'text',
                            'title'    => __( 'Description of bottom item: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: FANS', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        //VIDEO SECTION
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Video section', 'redux-framework-demo' ),
            'id'               => 'opt_video_section',
            'desc'             => __( 'Settings video section', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-facetime-video'
            ) 
        );
            // VIDEO SECTION
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Video', 'redux-framework-demo' ),
                    'id'               => 'video_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'video_foto_url',
                            'type'     => 'media',
                            'title'    => __( 'Foto Video URL: ', 'redux-framework-demo' ),
                           
                        ),
                        array(
                            'id'       => 'video_url',
                            'type'     => 'text',
                            'title'    => __( 'Video URL: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: www.youtube.com/12iasd+', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // INFO VIDEO SECTION
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Info video', 'redux-framework-demo' ),
                    'id'               => 'info_video_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'video_title',
                            'type'     => 'text',
                            'title'    => __('Title text: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: AS A GAMES DEVELOPER WITH OVER', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'video_subtitle',
                            'type'     => 'text',
                            'title'    => __('Caption text: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 15 YEARS’ EXPERIENCE,', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'video_description',
                            'type'     => 'text',
                            'title'    => __('Description text: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: we pioneered the adoption of mobile and free-to-play formats. We’re now leading the natural next step in the evolution of game development? Watch the video to learn how,', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SECTION OUR SOLUTION
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section Our Solution', 'redux-framework-demo' ),
            'id'               => 'opt_our_solution',
            'desc'             => __( 'Settings Our Solution', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-check'
            ) 
        );
            //SECTION OUR SOLUTION
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Settings our solution', 'redux-framework-demo' ),
                    'id'               => 'our_solution_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'our_solution_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: OUR SOLUTIONS', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'our_solution_description',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: We are making it possible for players to own, trade, loan, customise and develop key components of a game. Transforming the free-to-play model will help increase retention and DAU.', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
        //SECTION DOWNLOADS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section info downloads', 'redux-framework-demo' ),
            'id'               => 'opt_info_downloads',
            'desc'             => __( 'Settings info downloads', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-download-alt'
            ) 
        );
            //SECTION INFO DOWNLOADS
            Redux::setSection( $opt_name, array(
                    'title'            => __('Info downloads', 'redux-framework-demo' ),
                    'id'               => 'our_downloads_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'our_downloads_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 100M | DOWNLOADS', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'our_downloads_caption',
                            'type'     => 'text',
                            'title'    => __('Caption: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: $1.1BN | MARKET POTENTIAL', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'our_downloads_description',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: What players win or buy in the game they own outright. The digital assets they create retain value inside or outside of the game. They choose what happens with their assets. We’re bringing blockchain to gamers, not gamers to blockchain.', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
        //SECTION LIVE FEED
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section live feed', 'redux-framework-demo' ),
            'id'               => 'opt_live_feed',
            'desc'             => __( 'Settings live feed', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-globe-alt'
            ) 
        );
            //SECTION LIVE FEED
            Redux::setSection( $opt_name, array(
                    'title'            => __('Live feed', 'redux-framework-demo' ),
                    'id'               => 'live_feed_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'live_feed_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: LIVE | FEED', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
        //SECTION ABOUT STICK SPORTS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section about stick sports', 'redux-framework-demo' ),
            'id'               => 'about_stick_sports',
            'desc'             => __( 'Settings about stick sports', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-group-alt'
            ) 
        );  
            //SECTION ABOUT STICK SPORTS
            Redux::setSection( $opt_name, array(
                    'title'            => __('About stick sports', 'redux-framework-demo' ),
                    'id'               => 'about_stick_sports_settings',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'about_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: ABOUT | STICK SPORTS LTD', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'about_description',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Established since 2004, we are a successful publisher delivering mobile sports games across both iOS and Android platforms. Our games are consistently featured by App Store and Google Play editors. Our cricket games are #1 in cricket-playing nations and our tennis apps have topped the sports games chart in 120 countries.', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
            //SECTION ABOUT STICK SPORTS
            Redux::setSection( $opt_name, array(
                    'title'            => __('Value left', 'redux-framework-demo' ),
                    'id'               => 'about_stick_sports_left',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'about_value_left',
                            'type'     => 'text',
                            'title'    => __('Value: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 73,000,000', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'about_description_left',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Mobile Downloads', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
            //SECTION ABOUT STICK SPORTS
            Redux::setSection( $opt_name, array(
                    'title'            => __('Value center', 'redux-framework-demo' ),
                    'id'               => 'about_stick_sports_center',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'about_value_center',
                            'type'     => 'text',
                            'title'    => __('Value: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 2,500,00', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'about_description_center',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Monthly Active Users', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
            //SECTION ABOUT STICK SPORTS
            Redux::setSection( $opt_name, array(
                    'title'            => __('Value right', 'redux-framework-demo' ),
                    'id'               => 'about_stick_sports_right,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'about_value_right',
                            'type'     => 'text',
                            'title'    => __('Value: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: 50,000', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'about_description_right',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Daily Downloads', 'redux-framework-demo' ),
                        ),
                        
                    )
                )
            );
        //SECTION TEAM STICK SPORTS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section team', 'redux-framework-demo' ),
            'id'               => 'team_stick_sports',
            'desc'             => __( 'Settings Team Stick Sports', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-smiley-alt'
            ) 
        ); 
            //SECTION ABOUT STICK SPORTS
            Redux::setSection( $opt_name, array(
                    'title'            => __('Section Team', 'redux-framework-demo' ),
                    'id'               => 'team_stick_sports_settings,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'team_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: MEET | THE TEAM', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        //SECTION LIVE PLAYERS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section Live Players', 'redux-framework-demo' ),
            'id'               => 'live_players_settings',
            'desc'             => __( 'Settings Live Players', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-graph'
            ) 
        );
            Redux::setSection( $opt_name, array(
                    'title'            => __('Live Players Info', 'redux-framework-demo' ),
                    'id'               => 'live_players_info,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'live_players_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: CURRENT PLAYERS - | LIVE', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'live_players_desc',
                            'type'     => 'text',
                            'title'    => __('Description: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Runs scored today', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'live_players_time',
                            'type'     => 'text',
                            'title'    => __('Time: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Resets at | 00:00 UTC', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
        //SECTION OWN THE GAME
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Section Own the game', 'redux-framework-demo' ),
            'id'               => 'own_game_settings',
            'desc'             => __( 'Settings Own the game', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-file-alt'
            ) 
        );
            Redux::setSection( $opt_name, array(
                    'title'            => __('Own the game', 'redux-framework-demo' ),
                    'id'               => 'own_the_game_info,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'own_the_game_title',
                            'type'     => 'text',
                            'title'    => __('Title: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: IT’S TIME FOR PLAYERS TO | OWN THE GAME.', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'own_the_game_button_text',
                            'type'     => 'text',
                            'title'    => __('Text of button: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Learn more', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'own_the_game_link_button',
                            'type'     => 'text',
                            'title'    => __('Link of button: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: www.google.com', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );
        //SECTION FOOTER
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Footer', 'redux-framework-demo' ),
            'id'               => 'footer_settings',
            'desc'             => __( 'Settings Footer', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-edit'
            ) 
        );
            Redux::setSection( $opt_name, array(
                    'title'            => __('Footer', 'redux-framework-demo' ),
                    'id'               => 'footer_info,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'footer_logo',
                            'type'     => 'media',
                            'title'    => __('Footer logo: ', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'footer_contact_mail',
                            'type'     => 'text',
                            'title'    => __('Contact e-mail: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: enquiries@sticksportes.com', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'footer_head_office',
                            'type'     => 'text',
                            'title'    => __('Head Office: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Stick Sports Ltd, 90 York Way, London N1 6AG', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'footer_copyright',
                            'type'     => 'text',
                            'title'    => __('Copyright text: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: Stick Sports Ltd 2018 ALL Rights Reserved.', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );
        //GENERAL SETTINGS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'General settings', 'redux-framework-demo' ),
            'id'               => 'general_settings',
            'desc'             => __( 'General settings', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-cog'
            ) 
        );
             Redux::setSection( $opt_name, array(
                    'title'            => __('Social Networks', 'redux-framework-demo' ),
                    'id'               => 'social_networks_settings,',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'social_facebook',
                            'type'     => 'text',
                            'title'    => __('Facebook: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: www.facebook.com/stickSports', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_twitter',
                            'type'     => 'text',
                            'title'    => __('Twitter: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://twitter.com/stickSports', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_instagram',
                            'type'     => 'text',
                            'title'    => __('Instagram: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://instagram.com/sticksports', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_linkedin',
                            'type'     => 'text',
                            'title'    => __('Linkedin: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://www.linkedin.com/company/stick-sports-ltd', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_medium',
                            'type'     => 'text',
                            'title'    => __('Medium: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://medium.com/stick-sports', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_bitcoin',
                            'type'     => 'text',
                            'title'    => __('Bitcoin: ', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'social_reddit',
                            'type'     => 'text',
                            'title'    => __('Reddit: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://www.reddit.com/', 'redux-framework-demo' ),
                        ), 

                        array(
                            'id'       => 'social_telegram',
                            'type'     => 'text',
                            'title'    => __('Telegram: ', 'redux-framework-demo' ),
                            'desc'    => __( 'ex: https://www.telegram.com/', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

          

   

    /*********************************************
                 //FUNÇÕES//
    **********************************************/
        Redux::setSection( $opt_name, array(
            'icon'            => 'el el-list-alt',
            'title'           => __( 'Customizer Only', 'redux-framework-demo' ),
            'desc'            => __( '<p class="description">This Section should be visible only in Customizer</p>', 'redux-framework-demo' ),
            'customizer_only' => true,
            'fields'          => array(
                array(
                    'id'              => 'opt-customizer-only',
                    'type'            => 'select',
                    'title'           => __( 'Customizer Only Option', 'redux-framework-demo' ),
                    'subtitle'        => __( 'The subtitle is NOT visible in customizer', 'redux-framework-demo' ),
                    'desc'            => __( 'The field desc is NOT visible in customizer.', 'redux-framework-demo' ),
                    'customizer_only' => true,
                    //Must provide key => value pairs for select options
                    'options'         => array(
                        '1' => 'Opt 1',
                        '2' => 'Opt 2',
                        '3' => 'Opt 3'
                    ),
                    'default'         => '2'
                ),
            )
        ) );




        /*
         * <--- END SECTIONS
         */


        /*
         *
         * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
         *
         */

        /*
        *
        * --> Action hook examples
        *
        */

        // If Redux is running as a plugin, this will remove the demo notice and links
        //add_action( 'redux/loaded', 'remove_demo' );

        // Function to test the compiler hook and demo CSS output.
        // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
        //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

        // Change the arguments after they've been declared, but before the panel is created
        //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

        // Change the default value of a field after it's been set, but before it's been useds
        //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

        // Dynamically add a section. Can be also used to modify sections/fields
        //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        if ( ! function_exists( 'compiler_action' ) ) {
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            }
        }

        /**
         * Custom function for the callback validation referenced above
         * */
        if ( ! function_exists( 'redux_validate_callback_function' ) ) {
            function redux_validate_callback_function( $field, $value, $existing_value ) {
                $error   = false;
                $warning = false;

                //do your validation
                if ( $value == 1 ) {
                    $error = true;
                    $value = $existing_value;
                } elseif ( $value == 2 ) {
                    $warning = true;
                    $value   = $existing_value;
                }

                $return['value'] = $value;

                if ( $error == true ) {
                    $return['error'] = $field;
                    $field['msg']    = 'your custom error message';
                }

                if ( $warning == true ) {
                    $return['warning'] = $field;
                    $field['msg']      = 'your custom warning message';
                }

                return $return;
            }
        }

        /**
         * Custom function for the callback referenced above
         */
        if ( ! function_exists( 'redux_my_custom_field' ) ) {
            function redux_my_custom_field( $field, $value ) {
                print_r( $field );
                echo '<br/>';
                print_r( $value );
            }
        }

        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        if ( ! function_exists( 'dynamic_section' ) ) {
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el el-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }
        }

        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        if ( ! function_exists( 'change_arguments' ) ) {
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }
        }

        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        if ( ! function_exists( 'change_defaults' ) ) {
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }
        }

        /**
         * Removes the demo link and the notice of integrated demo from the redux-framework plugin
         */
        if ( ! function_exists( 'remove_demo' ) ) {
            function remove_demo() {
                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }
        }

