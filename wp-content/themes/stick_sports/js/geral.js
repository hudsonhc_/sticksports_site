$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
    setTimeout(function(){  
    	$("#carrosselDepoimentos").owlCarousel({
            items : 4,
            dots: true,
            loop: false,
            lazyLoad: true,
            mouseDrag:true,
            touchDrag  : true,	       
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            smartSpeed: 450,
    	    //CARROSSEL RESPONSIVO
    	    responsiveClass:true,			    
            responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },
               
                991:{
                    items:3
                },
                1024:{
                    items:4
                },
                1440:{
                    items:5
                },
                			            
            }		    		   		    
        	    
        	});
    }, 4000);

    //CARROSSEL DE DESTAQUE
    $("#carrosselTeam").owlCarousel({
        items : 5,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,             
            responsive:{
                320:{
                    items:3
                },
                600:{
                    items:5
                },
               
                991:{
                    items:5
                },
                1024:{
                    items:5
                },
                1440:{
                    items:5
                },
                                        
            }                                
            
        });
    var carrosselTeam = $("#carrosselTeam").data('owlCarousel');
    $('.btnLeft').click(function(){ carrosselTeam.prev(); });
    $('.btnlRight').click(function(){ carrosselTeam.next(); });

    //*********************
    	//APP CARROSSEL DE TEAM 
    //*********************
    $(".meettheTeam .carrosselTeam .item").click(function(e){
  		let dataUrl  = $(this).attr("dataUrl");
  		let dataDescription  = $(this).attr("dataDescription");
  		let dataName  = $(this).attr("dataName");
        let dataFunction  = $(this).attr("dataFunction");
  		let dataLinkdin  = $(this).attr("dataLinkdin");
  
  		$("#imgTeam").attr("src", dataUrl);
  		$(".teamDescription").text(dataDescription);
  		$(".teamName").text(dataName);
        $(".teamFunction").text(dataFunction);
  		$("#linkedin").attr("href", dataLinkdin);

	});

    //*********************
        //MENU 
    //*********************
    
    $("#modalOpen").click(function(e){
        $("#modalLogin").fadeIn();
        $(".formModal").fadeIn();
    });
     $("#poUpModal").click(function(e){
        event.preventDefault();
        $("#modalLogin").fadeIn();
        $(".formModal").fadeIn();
    });
    $("#modalClose").click(function(e){
        $(".formModal").fadeOut();
        setTimeout(function(){
            $("#modalLogin").slideUp();
        }, 100);
    });

    $("#videoOpen").click(function(e){
        $(".modalVideo").fadeIn();
        $("#playVideo")[0].play();
       
    });
    $("#modalCloseVideo").click(function(e){
        $(".modalVideo").fadeOut();
        $("#playVideo")[0].stop();
    });


    $("#openMenu").click(function(e){
        $("#navMenu").addClass("open");
    });
    $("#closeMenu").click(function(e){
        $("#navMenu").removeClass("open");
    });
     $("ul .menu-item a").click(function(){
        setTimeout(function(){
         $("#navMenu").removeClass("open");
        }, 1000);
    });
    
   

    $.fn.isOnScreen = function(){
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };

        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();
        
        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();
        
        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };

    var testeMostrarVisita = false;
    $(window).scroll(function(){

        if ($('.countNumber').isOnScreen() == true && testeMostrarVisita == false) {
          
           testeMostrarVisita = true;
           setTimeout(function(){
               $(".countNumber  li span.number").each(function() {
                    var o = $(this);
                    $({Counter: 0}).animate({
                        Counter: o.attr("data-stop")
                    },{
                        duration: 5e3,
                        easing: "swing",
                        step: function(e) {
                            o.text(Math.ceil(e))
                        }
                    })
                });
                $({Counter: 0}).animate({
                    Counter: o.attr("data-stop")
                },{
                    duration: 5e3,
                    easing: "swing",
                    step: function(e) {
                        o.text(Math.ceil(e))
                    }
                });
            }, 1);

        }
    });
    
    setTimeout(function(){
        $( ".widget.widget_sendy_widget input").attr({
          placeholder: "Enter your email",
        });
    }, 1);

    $(".questions").click(function(e) {
        $(this).toggleClass("openDescripition");
    });

    $('.scrollTop a').click(function(e) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
        
    });
});