<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_stickSports_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8aD~L&9fkuIh(#7>OVW9Rp3 QZ~y-ABnYkm_u0@tC>+M*8Lpvwz>x:{9^z!7S{//');
define('SECURE_AUTH_KEY',  'Jrc*P7lfwYx!f0o&w?LuG2Zrq(/:untJhOO<6g<QxW!;y<o_KWVsP-3Q+UKaRb2 ');
define('LOGGED_IN_KEY',    ']y]W<geBFhP4MGY7J>,&C^D*Yfmc ,#&5hxd?{>?pLv9~hj8defb+,JP0Qe,G3Ds');
define('NONCE_KEY',        '[_4+=d_GE{xi|rvf*Z?T^lMK:=-MP%8ppbN/#+wXljS$,M.(gyUQz(E|!X$S%hZr');
define('AUTH_SALT',        'wLZo1 IPH)R&9DUY%vRm3{d+nY)(r[S[ 7,H^>P^/l(S[Di&dZWt~^o(r-VuoO?,');
define('SECURE_AUTH_SALT', 'g@hgHS?klKPUD7$9*d|W8*T/>D1,zYv1CX=:<ZP{kVGW<~R)W>50*GP}a5t@dK.4');
define('LOGGED_IN_SALT',   'PDKjdAG<6vP69$r2HW/[sp|aq {rFWb@,8 v99S[@(-H{l^N_bIx~IWGh5dggN~V');
define('NONCE_SALT',       'q>p]Z6_#ye1,zkrp(vQxSDP+uTe.Uk:HL*GAya`A,zX^w}SSps?9|%7VW?% fJ4x');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ss_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
