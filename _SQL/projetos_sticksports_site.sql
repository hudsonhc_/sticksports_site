-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 26-Set-2018 às 03:15
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_sticksports_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_events`
--

DROP TABLE IF EXISTS `ss_aiowps_events`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_failed_logins`
--

DROP TABLE IF EXISTS `ss_aiowps_failed_logins`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_aiowps_failed_logins`
--

INSERT INTO `ss_aiowps_failed_logins` (`id`, `user_id`, `user_login`, `failed_login_date`, `login_attempt_ip`) VALUES
(1, 1, 'sticksports', '2018-09-05 15:15:27', '::1'),
(2, 1, 'sticksports', '2018-09-07 06:43:25', '::1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_global_meta`
--

DROP TABLE IF EXISTS `ss_aiowps_global_meta`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_login_activity`
--

DROP TABLE IF EXISTS `ss_aiowps_login_activity`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_login_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_aiowps_login_activity`
--

INSERT INTO `ss_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'sticksports', '2018-09-05 15:15:33', '2018-09-05 18:00:30', '::1', '', ''),
(2, 1, 'sticksports', '2018-09-05 18:00:28', '2018-09-05 18:00:30', '::1', '', ''),
(3, 1, 'sticksports', '2018-09-05 18:01:01', '2018-09-17 12:26:02', '::1', '', ''),
(4, 1, 'sticksports', '2018-09-05 18:01:30', '2018-09-17 12:26:02', '::1', '', ''),
(5, 1, 'sticksports', '2018-09-06 16:40:48', '2018-09-17 12:26:02', '::1', '', ''),
(6, 1, 'sticksports', '2018-09-07 06:43:30', '2018-09-17 12:26:02', '::1', '', ''),
(7, 1, 'sticksports', '2018-09-17 12:26:02', '2018-09-17 12:26:02', '::1', '', ''),
(8, 1, 'sticksports', '2018-09-17 12:26:14', '0000-00-00 00:00:00', '::1', '', ''),
(9, 1, 'sticksports', '2018-09-24 11:29:37', '0000-00-00 00:00:00', '::1', '', ''),
(10, 1, 'sticksports', '2018-09-24 15:17:37', '0000-00-00 00:00:00', '::1', '', ''),
(11, 1, 'sticksports', '2018-09-25 12:46:57', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_login_lockdown`
--

DROP TABLE IF EXISTS `ss_aiowps_login_lockdown`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_aiowps_permanent_block`
--

DROP TABLE IF EXISTS `ss_aiowps_permanent_block`;
CREATE TABLE IF NOT EXISTS `ss_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_commentmeta`
--

DROP TABLE IF EXISTS `ss_commentmeta`;
CREATE TABLE IF NOT EXISTS `ss_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_comments`
--

DROP TABLE IF EXISTS `ss_comments`;
CREATE TABLE IF NOT EXISTS `ss_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_comments`
--

INSERT INTO `ss_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-09-04 14:15:28', '2018-09-04 17:15:28', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_links`
--

DROP TABLE IF EXISTS `ss_links`;
CREATE TABLE IF NOT EXISTS `ss_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_options`
--

DROP TABLE IF EXISTS `ss_options`;
CREATE TABLE IF NOT EXISTS `ss_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=456 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_options`
--

INSERT INTO `ss_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/sticksports_site', 'yes'),
(2, 'home', 'http://localhost/projetos/sticksports_site', 'yes'),
(3, 'blogname', 'Stick Sports', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'contato@hcdesenvolvimentos.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:11:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:39:\"base-stick-sports/base-stick-sports.php\";i:3;s:33:\"classic-editor/classic-editor.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:41:\"google-tag-manager/google-tag-manager.php\";i:6;s:21:\"meta-box/meta-box.php\";i:7;s:37:\"post-types-order/post-types-order.php\";i:8;s:29:\"sendy-widget/sendy-widget.php\";i:9;s:24:\"wordpress-seo/wp-seo.php\";i:10;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'stick_sports', 'yes'),
(41, 'stylesheet', 'stick_sports', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '12', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'ss_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:80:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:11:\"edit_blocks\";b:1;s:18:\"edit_others_blocks\";b:1;s:14:\"publish_blocks\";b:1;s:19:\"read_private_blocks\";b:1;s:11:\"read_blocks\";b:1;s:13:\"delete_blocks\";b:1;s:21:\"delete_private_blocks\";b:1;s:23:\"delete_published_blocks\";b:1;s:20:\"delete_others_blocks\";b:1;s:19:\"edit_private_blocks\";b:1;s:21:\"edit_published_blocks\";b:1;s:13:\"create_blocks\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:47:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:11:\"edit_blocks\";b:1;s:18:\"edit_others_blocks\";b:1;s:14:\"publish_blocks\";b:1;s:19:\"read_private_blocks\";b:1;s:11:\"read_blocks\";b:1;s:13:\"delete_blocks\";b:1;s:21:\"delete_private_blocks\";b:1;s:23:\"delete_published_blocks\";b:1;s:20:\"delete_others_blocks\";b:1;s:19:\"edit_private_blocks\";b:1;s:21:\"edit_published_blocks\";b:1;s:13:\"create_blocks\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:17:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:11:\"edit_blocks\";b:1;s:14:\"publish_blocks\";b:1;s:11:\"read_blocks\";b:1;s:13:\"delete_blocks\";b:1;s:23:\"delete_published_blocks\";b:1;s:21:\"edit_published_blocks\";b:1;s:13:\"create_blocks\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:6:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:11:\"read_blocks\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:49:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:11:\"edit_blocks\";b:1;s:18:\"edit_others_blocks\";b:1;s:14:\"publish_blocks\";b:1;s:19:\"read_private_blocks\";b:1;s:11:\"read_blocks\";b:1;s:13:\"delete_blocks\";b:1;s:21:\"delete_private_blocks\";b:1;s:23:\"delete_published_blocks\";b:1;s:20:\"delete_others_blocks\";b:1;s:19:\"edit_private_blocks\";b:1;s:21:\"edit_published_blocks\";b:1;s:13:\"create_blocks\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:48:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:11:\"edit_blocks\";b:1;s:18:\"edit_others_blocks\";b:1;s:14:\"publish_blocks\";b:1;s:19:\"read_private_blocks\";b:1;s:11:\"read_blocks\";b:1;s:13:\"delete_blocks\";b:1;s:21:\"delete_private_blocks\";b:1;s:23:\"delete_published_blocks\";b:1;s:20:\"delete_others_blocks\";b:1;s:19:\"edit_private_blocks\";b:1;s:21:\"edit_published_blocks\";b:1;s:13:\"create_blocks\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', '', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:14:\"sendy_widget-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:8:{i:1537902929;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1537903139;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1537938929;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1537982199;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537982287;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537985939;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537985941;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1536081467;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(163, 'new_admin_email', 'contato@hcdesenvolvimentos.com.br', 'yes'),
(449, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1537895824;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(450, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1537895825;s:7:\"checked\";a:1:{s:12:\"stick_sports\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(451, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1537895826;s:7:\"checked\";a:13:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.3.6\";s:39:\"base-stick-sports/base-stick-sports.php\";s:3:\"0.1\";s:33:\"classic-editor/classic-editor.php\";s:3:\"0.4\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.0.4\";s:41:\"google-tag-manager/google-tag-manager.php\";s:5:\"1.0.2\";s:23:\"gutenberg/gutenberg.php\";s:5:\"3.7.0\";s:9:\"hello.php\";s:3:\"1.7\";s:28:\"wysija-newsletters/index.php\";s:3:\"2.9\";s:21:\"meta-box/meta-box.php\";s:6:\"4.15.4\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.3.9\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.11\";s:29:\"sendy-widget/sendy-widget.php\";s:3:\"1.0\";s:24:\"wordpress-seo/wp-seo.php\";s:5:\"8.1.1\";}s:8:\"response\";a:3:{s:23:\"gutenberg/gutenberg.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:23:\"w.org/plugins/gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:6:\"plugin\";s:23:\"gutenberg/gutenberg.php\";s:11:\"new_version\";s:5:\"3.9.0\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/gutenberg/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/gutenberg.3.9.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/gutenberg/assets/icon-256x256.jpg?rev=1776042\";s:2:\"1x\";s:62:\"https://ps.w.org/gutenberg/assets/icon-128x128.jpg?rev=1776042\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/gutenberg/assets/banner-1544x500.jpg?rev=1718710\";s:2:\"1x\";s:64:\"https://ps.w.org/gutenberg/assets/banner-772x250.jpg?rev=1718710\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.15.5\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.15.5.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"8.3\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/wordpress-seo.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:5:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"classic-editor\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"0.4\";s:7:\"updated\";s:19:\"2018-08-07 20:28:31\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/classic-editor/0.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-02-03 16:12:23\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:9:\"gutenberg\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.7.0\";s:7:\"updated\";s:19:\"2018-09-02 22:21:06\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/gutenberg/3.7.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"2.9\";s:7:\"updated\";s:19:\"2018-09-25 08:34:06\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/translation/plugin/wysija-newsletters/2.9/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"6.3.1\";s:7:\"updated\";s:19:\"2018-02-03 16:01:18\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/6.3.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:9:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.3.6\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"0.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1750045\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1750045\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1750404\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1751803\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.0.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"google-tag-manager/google-tag-manager.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/google-tag-manager\";s:4:\"slug\";s:18:\"google-tag-manager\";s:6:\"plugin\";s:41:\"google-tag-manager/google-tag-manager.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/google-tag-manager/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/google-tag-manager.1.0.2.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:62:\"https://s.w.org/plugins/geopattern-icon/google-tag-manager.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:3:\"2.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:2:\"1x\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.9\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.11\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.11.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"sendy-widget/sendy-widget.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/sendy-widget\";s:4:\"slug\";s:12:\"sendy-widget\";s:6:\"plugin\";s:29:\"sendy-widget/sendy-widget.php\";s:11:\"new_version\";s:3:\"1.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/sendy-widget/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/sendy-widget.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://s.w.org/plugins/geopattern-icon/sendy-widget_456fab.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/sendy-widget/assets/banner-772x250.jpg?rev=936885\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(356, 'widget_sendy_widget', 'a:2:{i:2;a:4:{s:5:\"title\";s:1:\" \";s:8:\"sendyurl\";s:0:\"\";s:6:\"listid\";s:0:\"\";s:8:\"hidename\";s:2:\"on\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(446, '_site_transient_timeout_theme_roots', '1537897622', 'no'),
(447, '_site_transient_theme_roots', 'a:1:{s:12:\"stick_sports\";s:7:\"/themes\";}', 'no'),
(144, 'recently_activated', 'a:0:{}', 'yes'),
(373, 'wpseo_sitemap_questions_cache_validator', 'vQkD', 'no'),
(132, 'can_compress_scripts', '1', 'no'),
(252, 'google_tag_manager_id', '', 'yes'),
(255, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:4:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:4:\"team\";s:4:\"show\";s:9:\"solutions\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(256, 'CPT_configured', 'TRUE', 'yes'),
(264, 'wpseo_sitemap_attachment_cache_validator', '57vz6', 'no'),
(271, 'wpseo_sitemap_team_cache_validator', '6teqe', 'no'),
(274, 'wpseo_sitemap_solutions_cache_validator', '6hNmy', 'no'),
(267, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(268, 'wpseo_sitemap_nav_menu_item_cache_validator', '5MZSx', 'no'),
(139, 'current_theme', 'Stick Sports', 'yes'),
(140, 'theme_mods_stick_sports', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:12:\"stick_sports\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(152, 'classic-editor-replace', 'replace', 'yes'),
(199, 'wpseo_sitemap_1_cache_validator', '57vz1', 'no'),
(178, 'aiowpsec_db_version', '1.9', 'yes'),
(179, 'aio_wp_security_configs', 'a:87:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:0:\"\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";s:1:\"3\";s:24:\"aiowps_retry_time_period\";s:1:\"5\";s:26:\"aiowps_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"dmrr56aq6w17s93dua2y\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"nd162jmrg7wxnegdzjec\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:0:\"\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:0:\"\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:0:\"\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";}', 'yes'),
(247, 'wpseo_sitemap_post_cache_validator', 'totc', 'no'),
(248, 'wpseo_sitemap_category_cache_validator', '5PQ8b', 'no'),
(249, 'wpseo_sitemap_page_cache_validator', '5Ec5T', 'no'),
(188, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(191, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(196, 'installation_step', '16', 'yes'),
(197, 'wysija', 'YToxNzp7czo5OiJmcm9tX25hbWUiO3M6MTE6InN0aWNrc3BvcnRzIjtzOjEyOiJyZXBseXRvX25hbWUiO3M6MTE6InN0aWNrc3BvcnRzIjtzOjE1OiJlbWFpbHNfbm90aWZpZWQiO3M6MzM6ImNvbnRhdG9AaGNkZXNlbnZvbHZpbWVudG9zLmNvbS5iciI7czoxMDoiZnJvbV9lbWFpbCI7czoxNDoiaW5mb0Bsb2NhbGhvc3QiO3M6MTM6InJlcGx5dG9fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjE1OiJkZWZhdWx0X2xpc3RfaWQiO2k6MTtzOjE3OiJ0b3RhbF9zdWJzY3JpYmVycyI7czoxOiIxIjtzOjE2OiJpbXBvcnR3cF9saXN0X2lkIjtpOjI7czoxODoiY29uZmlybV9lbWFpbF9saW5rIjtpOjk7czoxMjoidXBsb2FkZm9sZGVyIjtzOjY0OiJDOlx3YW1wXHd3d1xwcm9qZXRvc1xzdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkc1x3eXNpamFcIjtzOjk6InVwbG9hZHVybCI7czo2OToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9zdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvIjtzOjE2OiJjb25maXJtX2VtYWlsX2lkIjtpOjI7czo5OiJpbnN0YWxsZWQiO2I6MTtzOjIwOiJtYW5hZ2Vfc3Vic2NyaXB0aW9ucyI7YjoxO3M6MTQ6Imluc3RhbGxlZF90aW1lIjtpOjE1MzYwODUxNDU7czoxNzoid3lzaWphX2RiX3ZlcnNpb24iO3M6MzoiMi45IjtzOjExOiJka2ltX2RvbWFpbiI7czo5OiJsb2NhbGhvc3QiO30=', 'yes'),
(193, 'wysija_post_type_updated', '1536085142', 'yes'),
(194, 'wysija_post_type_created', '1536085142', 'yes'),
(195, 'rewrite_rules', 'a:95:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=12&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(454, '_transient_timeout_users_online', '1537903608', 'no'),
(455, '_transient_users_online', 'a:1:{i:0;a:3:{s:7:\"user_id\";i:1;s:13:\"last_activity\";d:1537891008;s:10:\"ip_address\";s:3:\"::1\";}}', 'no'),
(400, '_site_transient_timeout_browser_8651940b33fd1e958c905441aa40a03d', '1538404179', 'no'),
(401, '_site_transient_browser_8651940b33fd1e958c905441aa40a03d', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"69.0.3497.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(423, '_transient_timeout_wpseo-statistics-totals', '1537901714', 'no'),
(424, '_transient_wpseo-statistics-totals', 'a:1:{i:1;a:2:{s:6:\"scores\";a:0:{}s:8:\"division\";b:0;}}', 'no'),
(438, '_transient_timeout_select2-css_style_cdn_is_up', '1537976841', 'no'),
(439, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(440, '_transient_timeout_select2-js_script_cdn_is_up', '1537976841', 'no'),
(441, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(180, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.0.4\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1536074339;s:7:\"version\";s:5:\"5.0.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(187, '_transient_timeout_wpseo_meta_table_inaccessible', '1567621141', 'no'),
(185, '_transient_timeout_wpseo_link_table_inaccessible', '1567621141', 'no'),
(186, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(181, 'wpseo', 'a:19:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:5:\"8.1.1\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1536085140;}', 'yes');
INSERT INTO `ss_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(182, 'wpseo_titles', 'a:80:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:14:\"title-destaque\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:17:\"metadesc-destaque\";s:0:\"\";s:16:\"noindex-destaque\";b:0;s:17:\"showdate-destaque\";b:0;s:27:\"display-metabox-pt-destaque\";b:1;s:24:\"title-ptarchive-destaque\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-ptarchive-destaque\";s:0:\"\";s:26:\"bctitle-ptarchive-destaque\";s:0:\"\";s:26:\"noindex-ptarchive-destaque\";b:0;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:27:\"title-tax-categoriaDestaque\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:30:\"metadesc-tax-categoriaDestaque\";s:0:\"\";s:37:\"display-metabox-tax-categoriaDestaque\";b:1;s:29:\"noindex-tax-categoriaDestaque\";b:0;s:23:\"post_types-post-maintax\";i:0;s:27:\"post_types-destaque-maintax\";i:0;s:35:\"taxonomy-categoriaDestaque-ptparent\";i:0;}', 'yes'),
(183, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(184, 'wpseo_flush_rewrite', '1', 'yes'),
(216, 'wysija_last_php_cron_call', '1537902598', 'yes'),
(214, 'wysija_check_pn', '1537901363.835', 'yes'),
(198, 'wysija_reinstall', '0', 'no'),
(200, 'wpseo_sitemap_wysijap_cache_validator', '5LxDq', 'no'),
(201, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1537904963;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1536171601;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1537975707;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1538403989;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1538504401;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}}', 'yes'),
(203, 'redux_version_upgraded_from', '3.6.11', 'yes'),
(211, 'category_children', 'a:0:{}', 'yes'),
(205, 'configuracao-transients', 'a:4:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1537890461;s:13:\"last_compiler\";i:1536181379;s:11:\"last_import\";i:1536181379;}', 'yes'),
(204, 'configuracao', 'a:56:{s:8:\"last_tab\";s:1:\"1\";s:11:\"header_logo\";a:9:{s:3:\"url\";s:93:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo-sticksports@1x.png\";s:2:\"id\";s:2:\"30\";s:6:\"height\";s:2:\"73\";s:5:\"width\";s:3:\"222\";s:9:\"thumbnail\";s:100:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo-sticksports@1x-150x73.png\";s:5:\"title\";s:19:\"logo sticksports@1x\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:24:\"header_login_button_text\";s:7:\"SIGN UP\";s:24:\"header_link_button_login\";s:1:\"#\";s:13:\"header_favico\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:13:\"initial_title\";s:71:\"our blockchain-enabled platform, gives in-game assets real world value.\";s:19:\"initial_description\";s:124:\"We\'re creating an irresistible new free-to-play mobile sports game prtocol - redefining the way the industry rewards players\";s:38:\"initial_text_button_join_our_whitelist\";s:18:\"JOIN OUR WHITELIST\";s:19:\"initial_link_button\";s:1:\"#\";s:18:\"initial_link_check\";s:1:\"1\";s:15:\"community_title\";s:24:\"STICK SPORTS | COMMUNITY\";s:19:\"community_top_value\";s:4:\"450k\";s:25:\"community_top_description\";s:3:\"DAU\";s:22:\"community_middle_value\";s:3:\"55%\";s:28:\"community_middle_description\";s:22:\"Day one retention rate\";s:22:\"community_bottom_value\";s:5:\"800K+\";s:28:\"community_bottom_description\";s:4:\"fans\";s:14:\"video_foto_url\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Capturar.png\";s:2:\"id\";s:2:\"90\";s:6:\"height\";s:3:\"441\";s:5:\"width\";s:3:\"853\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Capturar-150x150.png\";s:5:\"title\";s:8:\"Capturar\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:9:\"video_url\";s:98:\"http://cricketstikers.handgran.com.br/wp-content/uploads/2018/09/Stick-Tennis-Tour-2.0-trailer.mp4\";s:11:\"video_title\";s:30:\"AS A GAMES DEVELOPER WITH OVER\";s:14:\"video_subtitle\";s:23:\"15 YEARS’ EXPERIENCE,\";s:17:\"video_description\";s:171:\"we pioneered the adoption of mobile and free-to-play formats. We’re now leading the natural next step in the evolution of game development? Watch the video to learn how,\";s:18:\"our_solution_title\";s:16:\" OUR | SOLUTIONS\";s:24:\"our_solution_description\";s:180:\"We are making it possible for players to own, trade, loan, customise and develop key components of a game. Transforming the free-to-play model will help increase retention and DAU.\";s:19:\"our_downloads_title\";s:16:\"100M | DOWNLOADS\";s:21:\"our_downloads_caption\";s:25:\"$1.1BN | MARKET POTENTIAL\";s:25:\"our_downloads_description\";s:123:\"e\'re launching out first blockchain-enabled game with the cricket obessed fans in the booming mobile games market in India.\";s:15:\"live_feed_title\";s:11:\"LIVE | FEED\";s:11:\"about_title\";s:24:\"ABOUT | STICK SPORTS LTD\";s:17:\"about_description\";s:322:\"Established since 2004, we are a successful publisher delivering mobile sports games across both iOS and Android platforms. Our games are consistently featured by App Store and Google Play editors. Our cricket games are #1 in cricket-playing nations and our tennis apps have topped the sports games chart in 120 countries.\";s:16:\"about_value_left\";s:8:\"73000000\";s:22:\"about_description_left\";s:16:\"Mobile Downloads\";s:18:\"about_value_center\";s:6:\"250000\";s:24:\"about_description_center\";s:20:\"Monthly Active Users\";s:17:\"about_value_right\";s:5:\"50000\";s:23:\"about_description_right\";s:15:\"Daily Downloads\";s:10:\"team_title\";s:15:\"MEET | THE TEAM\";s:18:\"live_players_title\";s:24:\"CURRENT PLAYERS - | LIVE\";s:17:\"live_players_desc\";s:17:\"Runs scored today\";s:17:\"live_players_time\";s:19:\"sets at | 00:00 UTC\";s:18:\"own_the_game_title\";s:42:\"IT’S TIME FOR PLAYERS TO | OWN THE GAME.\";s:24:\"own_the_game_button_text\";s:10:\"Learn more\";s:24:\"own_the_game_link_button\";s:1:\"#\";s:11:\"footer_logo\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo.png\";s:2:\"id\";s:2:\"32\";s:6:\"height\";s:3:\"219\";s:5:\"width\";s:3:\"666\";s:9:\"thumbnail\";s:86:\"http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo-150x150.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"footer_contact_mail\";s:26:\"enquiries@sticksportes.com\";s:18:\"footer_head_office\";s:44:\"Stick Sports Ltd, 90 York Way, London N1 6AG\";s:16:\"footer_copyright\";s:42:\"Stick Sports Ltd 2018 ALL Rights Reserved.\";s:15:\"social_facebook\";s:37:\"https://www.facebook.com/sticksports/\";s:14:\"social_twitter\";s:31:\"https://twitter.com/sticksports\";s:16:\"social_instagram\";s:38:\"https://www.instagram.com/sticksports/\";s:15:\"social_linkedin\";s:50:\"https://www.linkedin.com/company/stick-sports-ltd/\";s:13:\"social_medium\";s:31:\"https://medium.com/stick-sports\";s:14:\"social_bitcoin\";s:0:\"\";s:13:\"social_reddit\";s:1:\"#\";s:15:\"social_telegram\";s:32:\"https://t.me/sticksportsofficial\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(215, 'wysija_last_scheduled_check', '1537901363', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_postmeta`
--

DROP TABLE IF EXISTS `ss_postmeta`;
CREATE TABLE IF NOT EXISTS `ss_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=254 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_postmeta`
--

INSERT INTO `ss_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_oembed_bf6964af303ed7ed4297755ba99a9f33', '<iframe src=\"https://player.vimeo.com/video/22439234?app_id=122963\" width=\"640\" height=\"360\" frameborder=\"0\" title=\"The Mountain\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),
(4, 5, '_oembed_time_bf6964af303ed7ed4297755ba99a9f33', '1536081571'),
(5, 8, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(6, 8, '_mail', 'a:8:{s:7:\"subject\";s:29:\"Stick Sports \"[your-subject]\"\";s:6:\"sender\";s:47:\"[your-name] <contato@hcdesenvolvimentos.com.br>\";s:4:\"body\";s:193:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Stick Sports (http://localhost/projetos/sticksports_site)\";s:9:\"recipient\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(7, 8, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:29:\"Stick Sports \"[your-subject]\"\";s:6:\"sender\";s:48:\"Stick Sports <contato@hcdesenvolvimentos.com.br>\";s:4:\"body\";s:135:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Stick Sports (http://localhost/projetos/sticksports_site)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:43:\"Reply-To: contato@hcdesenvolvimentos.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(8, 8, '_messages', 'a:8:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";}'),
(9, 8, '_additional_settings', NULL),
(10, 8, '_locale', 'en_US'),
(11, 5, '_wp_trash_meta_status', 'draft'),
(12, 5, '_wp_trash_meta_time', '1536172522'),
(13, 5, '_wp_desired_post_slug', ''),
(14, 1, '_wp_trash_meta_status', 'publish'),
(15, 1, '_wp_trash_meta_time', '1536172522'),
(16, 1, '_wp_desired_post_slug', 'ola-mundo'),
(17, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(18, 12, '_edit_last', '1'),
(19, 12, '_wp_page_template', 'pg-template/inicial.php'),
(20, 12, '_yoast_wpseo_content_score', '30'),
(21, 12, '_edit_lock', '1536172688:1'),
(22, 14, '_wp_attached_file', '2018/09/appleStore.png'),
(23, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:282;s:6:\"height\";i:315;s:4:\"file\";s:22:\"2018/09/appleStore.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"appleStore-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"appleStore-269x300.png\";s:5:\"width\";i:269;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(24, 15, '_wp_attached_file', '2018/09/icon3.png'),
(25, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:81;s:6:\"height\";i:120;s:4:\"file\";s:17:\"2018/09/icon3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(26, 16, '_wp_attached_file', '2018/09/nuvem.png'),
(27, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1761;s:6:\"height\";i:1518;s:4:\"file\";s:17:\"2018/09/nuvem.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"nuvem-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"nuvem-300x259.png\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"nuvem-768x662.png\";s:5:\"width\";i:768;s:6:\"height\";i:662;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"nuvem-1024x883.png\";s:5:\"width\";i:1024;s:6:\"height\";i:883;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:17:\"nuvem-600x517.png\";s:5:\"width\";i:600;s:6:\"height\";i:517;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 17, '_wp_attached_file', '2018/09/starativo.png'),
(29, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:15;s:6:\"height\";i:14;s:4:\"file\";s:21:\"2018/09/starativo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(30, 18, '_wp_attached_file', '2018/09/starpass.png'),
(31, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:17;s:6:\"height\";i:16;s:4:\"file\";s:20:\"2018/09/starpass.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(32, 19, '_wp_attached_file', '2018/09/appleStore-1.png'),
(33, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:40;s:6:\"height\";i:50;s:4:\"file\";s:24:\"2018/09/appleStore-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 20, '_wp_attached_file', '2018/09/Bitmap.png'),
(35, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1134;s:6:\"height\";i:638;s:4:\"file\";s:18:\"2018/09/Bitmap.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"Bitmap-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"Bitmap-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"Bitmap-768x432.png\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"Bitmap-1024x576.png\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:18:\"Bitmap-600x338.png\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(36, 21, '_wp_attached_file', '2018/09/btn.png'),
(37, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:139;s:6:\"height\";i:40;s:4:\"file\";s:15:\"2018/09/btn.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(38, 22, '_wp_attached_file', '2018/09/cloud-download.png'),
(39, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:587;s:6:\"height\";i:506;s:4:\"file\";s:26:\"2018/09/cloud-download.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"cloud-download-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"cloud-download-300x259.png\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(40, 23, '_wp_attached_file', '2018/09/envelope.png'),
(41, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1287;s:6:\"height\";i:579;s:4:\"file\";s:20:\"2018/09/envelope.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"envelope-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"envelope-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"envelope-768x346.png\";s:5:\"width\";i:768;s:6:\"height\";i:346;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"envelope-1024x461.png\";s:5:\"width\";i:1024;s:6:\"height\";i:461;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:20:\"envelope-600x270.png\";s:5:\"width\";i:600;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 24, '_wp_attached_file', '2018/09/icon1-1.png'),
(43, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:219;s:6:\"height\";i:201;s:4:\"file\";s:19:\"2018/09/icon1-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"icon1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 25, '_wp_attached_file', '2018/09/icon2-2.png'),
(45, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:303;s:6:\"height\";i:186;s:4:\"file\";s:19:\"2018/09/icon2-2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"icon2-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"icon2-2-300x184.png\";s:5:\"width\";i:300;s:6:\"height\";i:184;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 26, '_wp_attached_file', '2018/09/icon3-1.png'),
(47, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:80;s:4:\"file\";s:19:\"2018/09/icon3-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(48, 27, '_wp_attached_file', '2018/09/iconCalendar.png'),
(49, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:24:\"2018/09/iconCalendar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(50, 28, '_wp_attached_file', '2018/09/iconUser.png'),
(51, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:68;s:6:\"height\";i:68;s:4:\"file\";s:20:\"2018/09/iconUser.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(52, 29, '_wp_attached_file', '2018/09/John_Feminella.jpg'),
(53, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2400;s:6:\"height\";i:1600;s:4:\"file\";s:26:\"2018/09/John_Feminella.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"John_Feminella-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"John_Feminella-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"John_Feminella-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"John_Feminella-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"John_Feminella-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"4\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:21:\"Canon EOS 5D Mark III\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1425080874\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"92\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:9:\"0.0015625\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(54, 30, '_wp_attached_file', '2018/09/logo-sticksports@1x.png'),
(55, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:222;s:6:\"height\";i:73;s:4:\"file\";s:31:\"2018/09/logo-sticksports@1x.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"logo-sticksports@1x-150x73.png\";s:5:\"width\";i:150;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(56, 31, '_wp_attached_file', '2018/09/logo-sticksports@2x.png'),
(57, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:444;s:6:\"height\";i:146;s:4:\"file\";s:31:\"2018/09/logo-sticksports@2x.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"logo-sticksports@2x-150x146.png\";s:5:\"width\";i:150;s:6:\"height\";i:146;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"logo-sticksports@2x-300x99.png\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(58, 32, '_wp_attached_file', '2018/09/logo.png'),
(59, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:666;s:6:\"height\";i:219;s:4:\"file\";s:16:\"2018/09/logo.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x99.png\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:16:\"logo-600x197.png\";s:5:\"width\";i:600;s:6:\"height\";i:197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(60, 33, '_wp_attached_file', '2018/09/New-Play-Store-logo.png'),
(61, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:384;s:6:\"height\";i:384;s:4:\"file\";s:31:\"2018/09/New-Play-Store-logo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"New-Play-Store-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"New-Play-Store-logo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(62, 34, '_wp_attached_file', '2018/09/nuvem-1.png'),
(63, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1174;s:6:\"height\";i:1012;s:4:\"file\";s:19:\"2018/09/nuvem-1.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"nuvem-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"nuvem-1-300x259.png\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"nuvem-1-768x662.png\";s:5:\"width\";i:768;s:6:\"height\";i:662;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"nuvem-1-1024x883.png\";s:5:\"width\";i:1024;s:6:\"height\";i:883;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:19:\"nuvem-1-600x517.png\";s:5:\"width\";i:600;s:6:\"height\";i:517;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(64, 35, '_wp_attached_file', '2018/09/Oval-4-Copy-2@2x.png'),
(65, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1044;s:6:\"height\";i:2146;s:4:\"file\";s:28:\"2018/09/Oval-4-Copy-2@2x.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"Oval-4-Copy-2@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"Oval-4-Copy-2@2x-146x300.png\";s:5:\"width\";i:146;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"Oval-4-Copy-2@2x-768x1579.png\";s:5:\"width\";i:768;s:6:\"height\";i:1579;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"Oval-4-Copy-2@2x-498x1024.png\";s:5:\"width\";i:498;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:29:\"Oval-4-Copy-2@2x-600x1233.png\";s:5:\"width\";i:600;s:6:\"height\";i:1233;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(66, 36, '_wp_attached_file', '2018/09/Oval-4@2x.png'),
(67, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1076;s:6:\"height\";i:2146;s:4:\"file\";s:21:\"2018/09/Oval-4@2x.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Oval-4@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"Oval-4@2x-150x300.png\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"Oval-4@2x-768x1532.png\";s:5:\"width\";i:768;s:6:\"height\";i:1532;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"Oval-4@2x-513x1024.png\";s:5:\"width\";i:513;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"Oval-4@2x-600x1197.png\";s:5:\"width\";i:600;s:6:\"height\";i:1197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(68, 37, '_wp_attached_file', '2018/09/Oval4.png'),
(69, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:538;s:6:\"height\";i:1073;s:4:\"file\";s:17:\"2018/09/Oval4.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"Oval4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"Oval4-150x300.png\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"Oval4-513x1024.png\";s:5:\"width\";i:513;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(70, 38, '_wp_attached_file', '2018/09/ovalFullLeft.png'),
(71, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1566;s:6:\"height\";i:3219;s:4:\"file\";s:24:\"2018/09/ovalFullLeft.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"ovalFullLeft-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"ovalFullLeft-146x300.png\";s:5:\"width\";i:146;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"ovalFullLeft-768x1579.png\";s:5:\"width\";i:768;s:6:\"height\";i:1579;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"ovalFullLeft-498x1024.png\";s:5:\"width\";i:498;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"ovalFullLeft-600x1233.png\";s:5:\"width\";i:600;s:6:\"height\";i:1233;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(72, 39, '_wp_attached_file', '2018/09/ovalFullRight.png'),
(73, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1614;s:6:\"height\";i:3219;s:4:\"file\";s:25:\"2018/09/ovalFullRight.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"ovalFullRight-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"ovalFullRight-150x300.png\";s:5:\"width\";i:150;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"ovalFullRight-768x1532.png\";s:5:\"width\";i:768;s:6:\"height\";i:1532;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"ovalFullRight-513x1024.png\";s:5:\"width\";i:513;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:26:\"ovalFullRight-600x1197.png\";s:5:\"width\";i:600;s:6:\"height\";i:1197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(74, 40, '_wp_attached_file', '2018/09/PaulCollins.jpg'),
(75, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:375;s:6:\"height\";i:500;s:4:\"file\";s:23:\"2018/09/PaulCollins.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"PaulCollins-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"PaulCollins-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(76, 41, '_wp_attached_file', '2018/09/peopleFace.jpg'),
(77, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:180;s:4:\"file\";s:22:\"2018/09/peopleFace.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"peopleFace-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"peopleFace-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(78, 42, '_wp_attached_file', '2018/09/playstore.png'),
(79, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:282;s:6:\"height\";i:315;s:4:\"file\";s:21:\"2018/09/playstore.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"playstore-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"playstore-269x300.png\";s:5:\"width\";i:269;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(80, 43, '_wp_attached_file', '2018/09/ponto.png'),
(81, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:39;s:6:\"height\";i:39;s:4:\"file\";s:17:\"2018/09/ponto.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(82, 44, '_wp_attached_file', '2018/09/purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf.png'),
(83, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:102:\"2018/09/purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:102:\"purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:102:\"purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(84, 45, '_wp_attached_file', '2018/09/Rectangle-3-Copy.png'),
(85, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1438;s:6:\"height\";i:338;s:4:\"file\";s:28:\"2018/09/Rectangle-3-Copy.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"Rectangle-3-Copy-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"Rectangle-3-Copy-300x71.png\";s:5:\"width\";i:300;s:6:\"height\";i:71;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"Rectangle-3-Copy-768x181.png\";s:5:\"width\";i:768;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"Rectangle-3-Copy-1024x241.png\";s:5:\"width\";i:1024;s:6:\"height\";i:241;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"Rectangle-3-Copy-600x141.png\";s:5:\"width\";i:600;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(86, 46, '_wp_attached_file', '2018/09/Rectangle-3-Copy@2x.png'),
(87, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2876;s:6:\"height\";i:676;s:4:\"file\";s:31:\"2018/09/Rectangle-3-Copy@2x.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@2x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"Rectangle-3-Copy@2x-300x71.png\";s:5:\"width\";i:300;s:6:\"height\";i:71;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@2x-768x181.png\";s:5:\"width\";i:768;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"Rectangle-3-Copy@2x-1024x241.png\";s:5:\"width\";i:1024;s:6:\"height\";i:241;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@2x-600x141.png\";s:5:\"width\";i:600;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(88, 47, '_wp_attached_file', '2018/09/Rectangle-3-Copy@3x.png'),
(89, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4314;s:6:\"height\";i:1014;s:4:\"file\";s:31:\"2018/09/Rectangle-3-Copy@3x.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@3x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"Rectangle-3-Copy@3x-300x71.png\";s:5:\"width\";i:300;s:6:\"height\";i:71;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@3x-768x181.png\";s:5:\"width\";i:768;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"Rectangle-3-Copy@3x-1024x241.png\";s:5:\"width\";i:1024;s:6:\"height\";i:241;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:31:\"Rectangle-3-Copy@3x-600x141.png\";s:5:\"width\";i:600;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(90, 48, '_wp_attached_file', '2018/09/right.png'),
(91, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:522;s:6:\"height\";i:1073;s:4:\"file\";s:17:\"2018/09/right.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"right-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"right-146x300.png\";s:5:\"width\";i:146;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"right-498x1024.png\";s:5:\"width\";i:498;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(92, 49, '_wp_attached_file', '2018/09/starativo-1.png'),
(93, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:30;s:6:\"height\";i:28;s:4:\"file\";s:23:\"2018/09/starativo-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(94, 50, '_wp_attached_file', '2018/09/starpass-1.png'),
(95, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:34;s:6:\"height\";i:32;s:4:\"file\";s:22:\"2018/09/starpass-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(96, 51, '_wp_attached_file', '2018/09/tablet.png'),
(97, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:954;s:4:\"file\";s:18:\"2018/09/tablet.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"tablet-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"tablet-236x300.png\";s:5:\"width\";i:236;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:18:\"tablet-600x763.png\";s:5:\"width\";i:600;s:6:\"height\";i:763;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(98, 52, '_menu_item_type', 'custom'),
(99, 52, '_menu_item_menu_item_parent', '0'),
(100, 52, '_menu_item_object_id', '52'),
(101, 52, '_menu_item_object', 'custom'),
(102, 52, '_menu_item_target', ''),
(103, 52, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(104, 52, '_menu_item_xfn', ''),
(105, 52, '_menu_item_url', 'http://#'),
(156, 58, 'StickSports_member_linkedin', '#'),
(107, 53, '_menu_item_type', 'custom'),
(108, 53, '_menu_item_menu_item_parent', '0'),
(109, 53, '_menu_item_object_id', '53'),
(110, 53, '_menu_item_object', 'custom'),
(111, 53, '_menu_item_target', ''),
(112, 53, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(113, 53, '_menu_item_xfn', ''),
(114, 53, '_menu_item_url', '#solution'),
(116, 54, '_menu_item_type', 'custom'),
(117, 54, '_menu_item_menu_item_parent', '0'),
(118, 54, '_menu_item_object_id', '54'),
(119, 54, '_menu_item_object', 'custom'),
(120, 54, '_menu_item_target', ''),
(121, 54, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(122, 54, '_menu_item_xfn', ''),
(123, 54, '_menu_item_url', '#roadmap'),
(155, 58, 'StickSports_member_position', 'Founder & CEO'),
(125, 55, '_menu_item_type', 'custom'),
(126, 55, '_menu_item_menu_item_parent', '0'),
(127, 55, '_menu_item_object_id', '55'),
(128, 55, '_menu_item_object', 'custom'),
(129, 55, '_menu_item_target', ''),
(130, 55, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(131, 55, '_menu_item_xfn', ''),
(132, 55, '_menu_item_url', '#team'),
(154, 58, '_thumbnail_id', '40'),
(134, 56, '_menu_item_type', 'custom'),
(135, 56, '_menu_item_menu_item_parent', '0'),
(136, 56, '_menu_item_object_id', '56'),
(137, 56, '_menu_item_object', 'custom'),
(138, 56, '_menu_item_target', ''),
(139, 56, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(140, 56, '_menu_item_xfn', ''),
(141, 56, '_menu_item_url', 'http://#'),
(153, 58, '_edit_lock', '1536176046:1'),
(143, 57, '_menu_item_type', 'custom'),
(144, 57, '_menu_item_menu_item_parent', '0'),
(145, 57, '_menu_item_object_id', '57'),
(146, 57, '_menu_item_object', 'custom'),
(147, 57, '_menu_item_target', ''),
(148, 57, '_menu_item_classes', 'a:1:{i:0;s:9:\"scrollTop\";}'),
(149, 57, '_menu_item_xfn', ''),
(150, 57, '_menu_item_url', 'http://#'),
(152, 58, '_edit_last', '1'),
(157, 58, '_yoast_wpseo_content_score', '60'),
(158, 59, '_edit_last', '1'),
(159, 59, '_edit_lock', '1536176049:1'),
(160, 59, '_thumbnail_id', '29'),
(161, 59, 'StickSports_member_position', 'Founder & CEO'),
(162, 59, '_yoast_wpseo_content_score', '60'),
(163, 60, '_edit_last', '1'),
(164, 60, '_edit_lock', '1536176234:1'),
(165, 60, '_thumbnail_id', '40'),
(166, 60, 'StickSports_member_position', 'Founder & CEO'),
(167, 60, 'StickSports_member_linkedin', '@'),
(168, 60, '_yoast_wpseo_content_score', '60'),
(169, 61, '_edit_last', '1'),
(170, 61, '_edit_lock', '1536176235:1'),
(171, 62, '_edit_last', '1'),
(172, 62, '_edit_lock', '1536176235:1'),
(173, 63, '_edit_last', '1'),
(174, 63, '_edit_lock', '1536176236:1'),
(175, 61, '_thumbnail_id', '40'),
(176, 61, 'StickSports_member_position', 'Founder & CEO'),
(177, 61, '_yoast_wpseo_content_score', '60'),
(178, 62, '_thumbnail_id', '29'),
(179, 62, 'StickSports_member_position', 'Founder & CEO'),
(180, 62, '_yoast_wpseo_content_score', '60'),
(181, 63, '_thumbnail_id', '40'),
(182, 63, 'StickSports_member_position', 'Founder & CEO'),
(183, 63, 'StickSports_member_linkedin', '#'),
(184, 63, '_yoast_wpseo_content_score', '60'),
(185, 64, '_edit_last', '1'),
(186, 64, '_edit_lock', '1536268906:1'),
(187, 64, '_thumbnail_id', '40'),
(188, 64, 'StickSports_member_position', 'Founder & CEO'),
(189, 64, 'StickSports_member_linkedin', 'https://linkedin.com.br'),
(190, 64, '_yoast_wpseo_content_score', '90'),
(191, 66, '_edit_last', '1'),
(192, 66, '_edit_lock', '1536178399:1'),
(193, 66, '_thumbnail_id', '25'),
(194, 66, '_yoast_wpseo_content_score', '90'),
(195, 67, '_edit_last', '1'),
(196, 67, '_edit_lock', '1536178450:1'),
(197, 67, '_yoast_wpseo_content_score', '90'),
(198, 67, '_thumbnail_id', '24'),
(199, 68, '_edit_last', '1'),
(200, 68, '_thumbnail_id', '25'),
(201, 68, '_yoast_wpseo_content_score', '90'),
(202, 68, '_edit_lock', '1536179474:1'),
(203, 71, '_edit_last', '1'),
(204, 71, '_wp_page_template', 'pg-template/faq.php'),
(205, 71, '_yoast_wpseo_content_score', '30'),
(206, 71, '_edit_lock', '1537203274:1'),
(207, 74, '_edit_last', '1'),
(208, 74, '_edit_lock', '1537211989:1'),
(209, 74, '_yoast_wpseo_content_score', '60'),
(210, 75, '_edit_last', '1'),
(211, 75, '_edit_lock', '1537212010:1'),
(212, 75, '_yoast_wpseo_content_score', '30'),
(213, 76, '_edit_last', '1'),
(214, 76, '_edit_lock', '1537212036:1'),
(215, 76, '_yoast_wpseo_content_score', '30'),
(216, 77, '_edit_last', '1'),
(217, 77, '_edit_lock', '1537212068:1'),
(218, 77, '_yoast_wpseo_content_score', '90'),
(219, 78, '_edit_last', '1'),
(220, 78, '_edit_lock', '1537212078:1'),
(221, 78, '_yoast_wpseo_content_score', '30'),
(222, 79, '_edit_last', '1'),
(223, 79, '_yoast_wpseo_content_score', '30'),
(224, 79, '_edit_lock', '1537212096:1'),
(225, 80, '_edit_last', '1'),
(226, 80, '_edit_lock', '1537212116:1'),
(227, 80, '_yoast_wpseo_content_score', '30'),
(228, 81, '_edit_last', '1'),
(229, 81, '_edit_lock', '1537212142:1'),
(230, 81, '_yoast_wpseo_content_score', '60'),
(231, 82, '_edit_last', '1'),
(232, 82, '_yoast_wpseo_content_score', '30'),
(233, 82, '_edit_lock', '1537212164:1'),
(234, 83, '_edit_last', '1'),
(235, 83, '_yoast_wpseo_content_score', '60'),
(236, 83, '_edit_lock', '1537212176:1'),
(237, 84, '_edit_last', '1'),
(238, 84, '_yoast_wpseo_content_score', '60'),
(239, 84, '_edit_lock', '1537212188:1'),
(240, 85, '_edit_last', '1'),
(241, 85, '_yoast_wpseo_content_score', '60'),
(242, 85, '_edit_lock', '1537212201:1'),
(243, 86, '_edit_last', '1'),
(244, 86, '_edit_lock', '1537212211:1'),
(245, 86, '_yoast_wpseo_content_score', '30'),
(246, 87, '_edit_last', '1'),
(247, 87, '_yoast_wpseo_content_score', '30'),
(248, 87, '_edit_lock', '1537212253:1'),
(249, 88, '_edit_last', '1'),
(250, 88, '_yoast_wpseo_content_score', '60'),
(251, 88, '_edit_lock', '1537218573:1'),
(252, 90, '_wp_attached_file', '2018/09/Capturar.png'),
(253, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:853;s:6:\"height\";i:441;s:4:\"file\";s:20:\"2018/09/Capturar.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Capturar-300x155.png\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"Capturar-768x397.png\";s:5:\"width\";i:768;s:6:\"height\";i:397;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:20:\"Capturar-600x310.png\";s:5:\"width\";i:600;s:6:\"height\";i:310;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_posts`
--

DROP TABLE IF EXISTS `ss_posts`;
CREATE TABLE IF NOT EXISTS `ss_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_posts`
--

INSERT INTO `ss_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-09-04 14:15:28', '2018-09-04 17:15:28', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'trash', 'open', 'open', '', 'ola-mundo__trashed', '', '', '2018-09-05 15:35:22', '2018-09-05 18:35:22', '', 0, 'http://localhost/projetos/sticksports_site/?p=1', 0, 'post', '', 1),
(2, 1, '2018-09-04 14:15:28', '2018-09-04 17:15:28', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins de semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href=\"http://localhost/projetos/sticksports_site/wp-admin/\">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2018-09-04 14:15:28', '2018-09-04 17:15:28', '', 0, 'http://localhost/projetos/sticksports_site/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-09-04 14:15:28', '2018-09-04 17:15:28', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/projetos/sticksports_site.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2018-09-04 14:15:28', '2018-09-04 17:15:28', '', 0, 'http://localhost/projetos/sticksports_site/?page_id=3', 0, 'page', '', 0),
(90, 1, '2018-09-25 12:47:38', '2018-09-25 15:47:38', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar', '', '', '2018-09-25 12:47:38', '2018-09-25 15:47:38', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Capturar.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2018-09-17 12:30:16', '2018-09-17 15:30:16', '', 'Faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2018-09-17 12:30:45', '2018-09-17 15:30:45', '', 0, 'http://localhost/projetos/sticksports_site/?page_id=71', 0, 'page', '', 0),
(5, 1, '2018-09-05 15:35:22', '2018-09-05 18:35:22', '<!-- wp:cover-image {\"url\":\"https://cldup.com/Fz-ASbo2s3.jpg\",\"align\":\"wide\"} -->\n<div class=\"wp-block-cover-image has-background-dim alignwide\" style=\"background-image:url(https://cldup.com/Fz-ASbo2s3.jpg)\"><p class=\"wp-block-cover-image-text\">Of Mountains &amp; Printing Presses</p></div>\n<!-- /wp:cover-image -->\n\n<!-- wp:paragraph -->\n<p>The goal of this new editor is to make adding rich content to WordPress simple and enjoyable. This whole post is composed of <em>pieces of content</em>—somewhat similar to LEGO bricks—that you can move around and interact with. Move your cursor around and you’ll notice the different blocks light up with outlines and arrows. Press the arrows to reposition blocks quickly, without fearing about losing things in the process of copying and pasting.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What you are reading now is a <strong>text block</strong> the most basic block of all. The text block has its own controls to be moved freely around the post...</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"right\"} -->\n<p style=\"text-align:right\">... like this one, which is right aligned.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Headings are separate blocks as well, which helps with the outline and organization of your content.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>A Picture is Worth a Thousand Words</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Handling images and media with the utmost care is a primary focus of the new editor. Hopefully, you’ll find aspects of adding captions or going full-width with your pictures much easier and robust than before.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"align\":\"center\"} -->\n<div class=\"wp-block-image\"><figure class=\"aligncenter\"><img src=\"https://cldup.com/cXyG__fTLN.jpg\" alt=\"Beautiful landscape\"/><figcaption>If your theme supports it, you’ll see the \"wide\" button on the image toolbar. Give it a try.</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Try selecting and removing or editing the caption, now you don’t have to be careful about selecting the image or other text by mistake and ruining the presentation.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>The <em>Inserter</em> Tool</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Imagine everything that WordPress can do is available to you quickly and in the same place on the interface. No need to figure out HTML tags, classes, or remember complicated shortcode syntax. That’s the spirit behind the inserter—the <code>(+)</code> button you’ll see around the editor—which allows you to browse all available content blocks and add them into your post. Plugins and themes are able to register their own, opening up all sort of possibilities for rich editing and publishing.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Go give it a try, you may discover things WordPress can already add into your posts that you didn’t know about. Here’s a short list of what you can currently find there:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n	<li>Text &amp; Headings</li>\n	<li>Images &amp; Videos</li>\n	<li>Galleries</li>\n	<li>Embeds, like YouTube, Tweets, or other WordPress posts.</li>\n	<li>Layout blocks, like Buttons, Hero Images, Separators, etc.</li>\n	<li>And <em>Lists</em> like this one of course :)</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Visual Editing</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"fontSize\":\"small\"} -->\n<p class=\"has-small-font-size\">A huge benefit of blocks is that you can edit them in place and manipulate your content directly. Instead of having fields for editing things like the source of a quote, or the text of a button, you can directly change the content. Try editing the following quote:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The editor will endeavor to create a new page and post building experience that makes writing rich posts effortless, and has “blocks” to make it easy what today might take shortcodes, custom HTML, or “mystery meat” embed discovery.</p><cite>Matt Mullenweg, 2017</cite></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The information corresponding to the source of the quote is a separate text field, similar to captions under images, so the structure of the quote is protected even if you select, modify, or remove the source. It’s always easy to add it back.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Blocks can be anything you need. For instance, you may want to add a subdued quote as part of the composition of your text, or you may prefer to display a giant stylized one. All of these options are available in the inserter.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:gallery {\"columns\":2} -->\n<ul class=\"wp-block-gallery columns-2 is-cropped\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/n0g6ME5VKC.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/ZjESfxPI3R.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/EKNF8xD2UM.jpg\" alt=\"\"/></figure></li></ul>\n<!-- /wp:gallery -->\n\n<!-- wp:paragraph -->\n<p>You can change the amount of columns in your galleries by dragging a slider in the block inspector in the sidebar.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Media Rich</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you combine the new <strong>wide</strong> and <strong>full-wide</strong> alignments with galleries, you can create a very media rich layout, very quickly:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"align\":\"full\"} -->\n<figure class=\"wp-block-image alignfull\"><img src=\"https://cldup.com/8lhI-gKnI2.jpg\" alt=\"Accessibility is important — don’t forget image alt attribute\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Sure, the full-wide image can be pretty big. But sometimes the image is worth it.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:gallery {\"align\":\"wide\"} -->\n<ul class=\"wp-block-gallery alignwide columns-2 is-cropped\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/_rSwtEeDGD.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/L-cC3qX2DN.jpg\" alt=\"\"/></figure></li></ul>\n<!-- /wp:gallery -->\n\n<!-- wp:paragraph -->\n<p>The above is a gallery with just two images. It’s an easier way to create visually appealing layouts, without having to deal with floats. You can also easily convert the gallery back to individual images again, by using the block switcher.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Any block can opt into these alignments. The embed block has them also, and is responsive out of the box:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/vimeo {\"url\":\"https://vimeo.com/22439234\",\"type\":\"video\",\"providerNameSlug\":\"vimeo\",\"align\":\"wide\"} -->\n<figure class=\"wp-block-embed-vimeo alignwide wp-block-embed is-type-video is-provider-vimeo\">\nhttps://vimeo.com/22439234\n</figure>\n<!-- /wp:core-embed/vimeo -->\n\n<!-- wp:paragraph -->\n<p>You can build any block you like, static or dynamic, decorative or plain. Here’s a pullquote block:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:pullquote -->\n<blockquote class=\"wp-block-pullquote\"><p>Code is Poetry</p><cite>The WordPress community</cite></blockquote>\n<!-- /wp:pullquote -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\">\n	<em>\n		If you want to learn more about how to build additional blocks, or if you are interested in helping with the project, head over to the <a href=\"%s\">GitHub repository</a>.	</em>\n</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:button {\"align\":\"center\"} -->\n<div class=\"wp-block-button aligncenter\"><a class=\"wp-block-button__link\" href=\"https://github.com/WordPress/gutenberg\">Help build Gutenberg</a></div>\n<!-- /wp:button -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\">Thanks for testing Gutenberg!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\"><img draggable=\"false\" class=\"emoji\" alt=\"👋\" src=\"https://s.w.org/images/core/emoji/2.3/svg/1f44b.svg\"/></p>\n<!-- /wp:paragraph -->', 'Welcome to the Gutenberg Editor', '', 'trash', 'open', 'open', '', '__trashed', '', '', '2018-09-05 15:35:22', '2018-09-05 18:35:22', '', 0, 'http://localhost/projetos/sticksports_site/?p=5', 0, 'post', '', 0),
(89, 1, '2018-09-24 14:53:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-09-24 14:53:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/sticksports_site/?p=89', 0, 'post', '', 0),
(8, 1, '2018-09-04 15:18:59', '2018-09-04 18:18:59', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]\nStick Sports \"[your-subject]\"\n[your-name] <contato@hcdesenvolvimentos.com.br>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Stick Sports (http://localhost/projetos/sticksports_site)\ncontato@hcdesenvolvimentos.com.br\nReply-To: [your-email]\n\n0\n0\n\nStick Sports \"[your-subject]\"\nStick Sports <contato@hcdesenvolvimentos.com.br>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Stick Sports (http://localhost/projetos/sticksports_site)\n[your-email]\nReply-To: contato@hcdesenvolvimentos.com.br\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-09-04 15:18:59', '2018-09-04 18:18:59', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=wpcf7_contact_form&p=8', 0, 'wpcf7_contact_form', '', 0),
(9, 1, '2018-09-04 15:19:05', '2018-09-04 18:19:05', '[wysija_page]', 'Subscription confirmation', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2018-09-04 15:19:05', '2018-09-04 18:19:05', '', 0, 'http://localhost/projetos/sticksports_site/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(10, 1, '2018-09-05 15:35:22', '2018-09-05 18:35:22', '<!-- wp:cover-image {\"url\":\"https://cldup.com/Fz-ASbo2s3.jpg\",\"align\":\"wide\"} -->\n<div class=\"wp-block-cover-image has-background-dim alignwide\" style=\"background-image:url(https://cldup.com/Fz-ASbo2s3.jpg)\"><p class=\"wp-block-cover-image-text\">Of Mountains &amp; Printing Presses</p></div>\n<!-- /wp:cover-image -->\n\n<!-- wp:paragraph -->\n<p>The goal of this new editor is to make adding rich content to WordPress simple and enjoyable. This whole post is composed of <em>pieces of content</em>—somewhat similar to LEGO bricks—that you can move around and interact with. Move your cursor around and you’ll notice the different blocks light up with outlines and arrows. Press the arrows to reposition blocks quickly, without fearing about losing things in the process of copying and pasting.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What you are reading now is a <strong>text block</strong> the most basic block of all. The text block has its own controls to be moved freely around the post...</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"right\"} -->\n<p style=\"text-align:right\">... like this one, which is right aligned.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Headings are separate blocks as well, which helps with the outline and organization of your content.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>A Picture is Worth a Thousand Words</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Handling images and media with the utmost care is a primary focus of the new editor. Hopefully, you’ll find aspects of adding captions or going full-width with your pictures much easier and robust than before.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"align\":\"center\"} -->\n<div class=\"wp-block-image\"><figure class=\"aligncenter\"><img src=\"https://cldup.com/cXyG__fTLN.jpg\" alt=\"Beautiful landscape\"/><figcaption>If your theme supports it, you’ll see the \"wide\" button on the image toolbar. Give it a try.</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Try selecting and removing or editing the caption, now you don’t have to be careful about selecting the image or other text by mistake and ruining the presentation.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>The <em>Inserter</em> Tool</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Imagine everything that WordPress can do is available to you quickly and in the same place on the interface. No need to figure out HTML tags, classes, or remember complicated shortcode syntax. That’s the spirit behind the inserter—the <code>(+)</code> button you’ll see around the editor—which allows you to browse all available content blocks and add them into your post. Plugins and themes are able to register their own, opening up all sort of possibilities for rich editing and publishing.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Go give it a try, you may discover things WordPress can already add into your posts that you didn’t know about. Here’s a short list of what you can currently find there:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n	<li>Text &amp; Headings</li>\n	<li>Images &amp; Videos</li>\n	<li>Galleries</li>\n	<li>Embeds, like YouTube, Tweets, or other WordPress posts.</li>\n	<li>Layout blocks, like Buttons, Hero Images, Separators, etc.</li>\n	<li>And <em>Lists</em> like this one of course :)</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:heading -->\n<h2>Visual Editing</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph {\"fontSize\":\"small\"} -->\n<p class=\"has-small-font-size\">A huge benefit of blocks is that you can edit them in place and manipulate your content directly. Instead of having fields for editing things like the source of a quote, or the text of a button, you can directly change the content. Try editing the following quote:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The editor will endeavor to create a new page and post building experience that makes writing rich posts effortless, and has “blocks” to make it easy what today might take shortcodes, custom HTML, or “mystery meat” embed discovery.</p><cite>Matt Mullenweg, 2017</cite></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The information corresponding to the source of the quote is a separate text field, similar to captions under images, so the structure of the quote is protected even if you select, modify, or remove the source. It’s always easy to add it back.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Blocks can be anything you need. For instance, you may want to add a subdued quote as part of the composition of your text, or you may prefer to display a giant stylized one. All of these options are available in the inserter.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:gallery {\"columns\":2} -->\n<ul class=\"wp-block-gallery columns-2 is-cropped\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/n0g6ME5VKC.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/ZjESfxPI3R.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/EKNF8xD2UM.jpg\" alt=\"\"/></figure></li></ul>\n<!-- /wp:gallery -->\n\n<!-- wp:paragraph -->\n<p>You can change the amount of columns in your galleries by dragging a slider in the block inspector in the sidebar.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading -->\n<h2>Media Rich</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you combine the new <strong>wide</strong> and <strong>full-wide</strong> alignments with galleries, you can create a very media rich layout, very quickly:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {\"align\":\"full\"} -->\n<figure class=\"wp-block-image alignfull\"><img src=\"https://cldup.com/8lhI-gKnI2.jpg\" alt=\"Accessibility is important — don’t forget image alt attribute\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Sure, the full-wide image can be pretty big. But sometimes the image is worth it.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:gallery {\"align\":\"wide\"} -->\n<ul class=\"wp-block-gallery alignwide columns-2 is-cropped\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/_rSwtEeDGD.jpg\" alt=\"\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://cldup.com/L-cC3qX2DN.jpg\" alt=\"\"/></figure></li></ul>\n<!-- /wp:gallery -->\n\n<!-- wp:paragraph -->\n<p>The above is a gallery with just two images. It’s an easier way to create visually appealing layouts, without having to deal with floats. You can also easily convert the gallery back to individual images again, by using the block switcher.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Any block can opt into these alignments. The embed block has them also, and is responsive out of the box:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:core-embed/vimeo {\"url\":\"https://vimeo.com/22439234\",\"type\":\"video\",\"providerNameSlug\":\"vimeo\",\"align\":\"wide\"} -->\n<figure class=\"wp-block-embed-vimeo alignwide wp-block-embed is-type-video is-provider-vimeo\">\nhttps://vimeo.com/22439234\n</figure>\n<!-- /wp:core-embed/vimeo -->\n\n<!-- wp:paragraph -->\n<p>You can build any block you like, static or dynamic, decorative or plain. Here’s a pullquote block:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:pullquote -->\n<blockquote class=\"wp-block-pullquote\"><p>Code is Poetry</p><cite>The WordPress community</cite></blockquote>\n<!-- /wp:pullquote -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\">\n	<em>\n		If you want to learn more about how to build additional blocks, or if you are interested in helping with the project, head over to the <a href=\"%s\">GitHub repository</a>.	</em>\n</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:button {\"align\":\"center\"} -->\n<div class=\"wp-block-button aligncenter\"><a class=\"wp-block-button__link\" href=\"https://github.com/WordPress/gutenberg\">Help build Gutenberg</a></div>\n<!-- /wp:button -->\n\n<!-- wp:separator -->\n<hr class=\"wp-block-separator\"/>\n<!-- /wp:separator -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\">Thanks for testing Gutenberg!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"center\"} -->\n<p style=\"text-align:center\"><img draggable=\"false\" class=\"emoji\" alt=\"👋\" src=\"https://s.w.org/images/core/emoji/2.3/svg/1f44b.svg\"/></p>\n<!-- /wp:paragraph -->', 'Welcome to the Gutenberg Editor', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-09-05 15:35:22', '2018-09-05 18:35:22', '', 5, 'http://localhost/projetos/sticksports_site/5-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-09-05 15:35:22', '2018-09-05 18:35:22', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-09-05 15:35:22', '2018-09-05 18:35:22', '', 1, 'http://localhost/projetos/sticksports_site/1-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2018-09-05 15:35:35', '2018-09-05 18:35:35', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-09-05 15:35:35', '2018-09-05 18:35:35', '', 0, 'http://localhost/projetos/sticksports_site/?page_id=12', 0, 'page', '', 0),
(13, 1, '2018-09-05 15:35:35', '2018-09-05 18:35:35', '', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2018-09-05 15:35:35', '2018-09-05 18:35:35', '', 12, 'http://localhost/projetos/sticksports_site/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2018-09-05 15:47:20', '2018-09-05 18:47:20', '', '_appleStore', '', 'inherit', 'open', 'closed', '', '_applestore', '', '', '2018-09-05 15:47:20', '2018-09-05 18:47:20', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/appleStore.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2018-09-05 15:47:22', '2018-09-05 18:47:22', '', '_icon3', '', 'inherit', 'open', 'closed', '', '_icon3', '', '', '2018-09-05 15:47:22', '2018-09-05 18:47:22', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/icon3.png', 0, 'attachment', 'image/png', 0),
(16, 1, '2018-09-05 15:47:24', '2018-09-05 18:47:24', '', '_nuvem', '', 'inherit', 'open', 'closed', '', '_nuvem', '', '', '2018-09-05 15:47:24', '2018-09-05 18:47:24', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/nuvem.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2018-09-05 15:47:27', '2018-09-05 18:47:27', '', '_starativo', '', 'inherit', 'open', 'closed', '', '_starativo', '', '', '2018-09-05 15:47:27', '2018-09-05 18:47:27', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/starativo.png', 0, 'attachment', 'image/png', 0),
(18, 1, '2018-09-05 15:47:29', '2018-09-05 18:47:29', '', '_starpass', '', 'inherit', 'open', 'closed', '', '_starpass', '', '', '2018-09-05 15:47:29', '2018-09-05 18:47:29', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/starpass.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2018-09-05 15:47:31', '2018-09-05 18:47:31', '', 'appleStore', '', 'inherit', 'open', 'closed', '', 'applestore', '', '', '2018-09-05 15:47:31', '2018-09-05 18:47:31', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/appleStore-1.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2018-09-05 15:47:32', '2018-09-05 18:47:32', '', 'Bitmap', '', 'inherit', 'open', 'closed', '', 'bitmap', '', '', '2018-09-05 15:47:32', '2018-09-05 18:47:32', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Bitmap.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2018-09-05 15:47:35', '2018-09-05 18:47:35', '', 'btn', '', 'inherit', 'open', 'closed', '', 'btn', '', '', '2018-09-05 15:47:35', '2018-09-05 18:47:35', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/btn.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2018-09-05 15:47:37', '2018-09-05 18:47:37', '', 'cloud-download', '', 'inherit', 'open', 'closed', '', 'cloud-download', '', '', '2018-09-05 15:47:37', '2018-09-05 18:47:37', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/cloud-download.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2018-09-05 15:47:39', '2018-09-05 18:47:39', '', 'envelope', '', 'inherit', 'open', 'closed', '', 'envelope', '', '', '2018-09-05 15:47:39', '2018-09-05 18:47:39', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/envelope.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2018-09-05 15:47:41', '2018-09-05 18:47:41', '', 'icon1', '', 'inherit', 'open', 'closed', '', 'icon1', '', '', '2018-09-05 15:47:41', '2018-09-05 18:47:41', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/icon1-1.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2018-09-05 15:47:43', '2018-09-05 18:47:43', '', 'icon2', '', 'inherit', 'open', 'closed', '', 'icon2', '', '', '2018-09-05 15:47:43', '2018-09-05 18:47:43', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/icon2-2.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2018-09-05 15:47:45', '2018-09-05 18:47:45', '', 'icon3', '', 'inherit', 'open', 'closed', '', 'icon3', '', '', '2018-09-05 15:47:45', '2018-09-05 18:47:45', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/icon3-1.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2018-09-05 15:47:46', '2018-09-05 18:47:46', '', 'iconCalendar', '', 'inherit', 'open', 'closed', '', 'iconcalendar', '', '', '2018-09-05 15:47:46', '2018-09-05 18:47:46', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/iconCalendar.png', 0, 'attachment', 'image/png', 0),
(28, 1, '2018-09-05 15:47:48', '2018-09-05 18:47:48', '', 'iconUser', '', 'inherit', 'open', 'closed', '', 'iconuser', '', '', '2018-09-05 15:47:48', '2018-09-05 18:47:48', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/iconUser.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2018-09-05 15:47:50', '2018-09-05 18:47:50', '', 'John_Feminella', '', 'inherit', 'open', 'closed', '', 'john_feminella', '', '', '2018-09-05 15:47:50', '2018-09-05 18:47:50', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/John_Feminella.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2018-09-05 15:47:53', '2018-09-05 18:47:53', '', 'logo sticksports@1x', '', 'inherit', 'open', 'closed', '', 'logo-sticksports1x', '', '', '2018-09-05 15:47:53', '2018-09-05 18:47:53', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo-sticksports@1x.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2018-09-05 15:47:54', '2018-09-05 18:47:54', '', 'logo sticksports@2x', '', 'inherit', 'open', 'closed', '', 'logo-sticksports2x', '', '', '2018-09-05 15:47:54', '2018-09-05 18:47:54', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo-sticksports@2x.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2018-09-05 15:47:56', '2018-09-05 18:47:56', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-09-05 15:47:56', '2018-09-05 18:47:56', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/logo.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2018-09-05 15:47:58', '2018-09-05 18:47:58', '', 'New-Play-Store-logo', '', 'inherit', 'open', 'closed', '', 'new-play-store-logo', '', '', '2018-09-05 15:47:58', '2018-09-05 18:47:58', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/New-Play-Store-logo.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2018-09-05 15:47:59', '2018-09-05 18:47:59', '', 'nuvem', '', 'inherit', 'open', 'closed', '', 'nuvem', '', '', '2018-09-05 15:47:59', '2018-09-05 18:47:59', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/nuvem-1.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2018-09-05 15:48:02', '2018-09-05 18:48:02', '', 'Oval 4 Copy 2@2x', '', 'inherit', 'open', 'closed', '', 'oval-4-copy-22x', '', '', '2018-09-05 15:48:02', '2018-09-05 18:48:02', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Oval-4-Copy-2@2x.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2018-09-05 15:48:06', '2018-09-05 18:48:06', '', 'Oval 4@2x', '', 'inherit', 'open', 'closed', '', 'oval-42x', '', '', '2018-09-05 15:48:06', '2018-09-05 18:48:06', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Oval-4@2x.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2018-09-05 15:48:10', '2018-09-05 18:48:10', '', 'Oval4', '', 'inherit', 'open', 'closed', '', 'oval4', '', '', '2018-09-05 15:48:10', '2018-09-05 18:48:10', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Oval4.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2018-09-05 15:48:12', '2018-09-05 18:48:12', '', 'ovalFullLeft', '', 'inherit', 'open', 'closed', '', 'ovalfullleft', '', '', '2018-09-05 15:48:12', '2018-09-05 18:48:12', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/ovalFullLeft.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2018-09-05 15:48:17', '2018-09-05 18:48:17', '', 'ovalFullRight', '', 'inherit', 'open', 'closed', '', 'ovalfullright', '', '', '2018-09-05 15:48:17', '2018-09-05 18:48:17', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/ovalFullRight.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2018-09-05 15:48:23', '2018-09-05 18:48:23', '', 'PaulCollins', '', 'inherit', 'open', 'closed', '', 'paulcollins', '', '', '2018-09-05 15:48:23', '2018-09-05 18:48:23', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/PaulCollins.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2018-09-05 15:48:25', '2018-09-05 18:48:25', '', 'peopleFace', '', 'inherit', 'open', 'closed', '', 'peopleface', '', '', '2018-09-05 15:48:25', '2018-09-05 18:48:25', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/peopleFace.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2018-09-05 15:48:26', '2018-09-05 18:48:26', '', 'playstore', '', 'inherit', 'open', 'closed', '', 'playstore', '', '', '2018-09-05 15:48:26', '2018-09-05 18:48:26', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/playstore.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2018-09-05 15:48:28', '2018-09-05 18:48:28', '', 'ponto', '', 'inherit', 'open', 'closed', '', 'ponto', '', '', '2018-09-05 15:48:28', '2018-09-05 18:48:28', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/ponto.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2018-09-05 15:48:30', '2018-09-05 18:48:30', '', 'purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf', '', 'inherit', 'open', 'closed', '', 'purepng-com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf', '', '', '2018-09-05 15:48:30', '2018-09-05 18:48:30', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/purepng.com-app-store-icon-ios-7symbolsiconsapple-iosiosios-7-iconsios-7-721522596490omztf.png', 0, 'attachment', 'image/png', 0),
(45, 1, '2018-09-05 15:48:31', '2018-09-05 18:48:31', '', 'Rectangle 3 Copy', '', 'inherit', 'open', 'closed', '', 'rectangle-3-copy', '', '', '2018-09-05 15:48:31', '2018-09-05 18:48:31', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Rectangle-3-Copy.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2018-09-05 15:48:33', '2018-09-05 18:48:33', '', 'Rectangle 3 Copy@2x', '', 'inherit', 'open', 'closed', '', 'rectangle-3-copy2x', '', '', '2018-09-05 15:48:33', '2018-09-05 18:48:33', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Rectangle-3-Copy@2x.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2018-09-05 15:48:36', '2018-09-05 18:48:36', '', 'Rectangle 3 Copy@3x', '', 'inherit', 'open', 'closed', '', 'rectangle-3-copy3x', '', '', '2018-09-05 15:48:36', '2018-09-05 18:48:36', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/Rectangle-3-Copy@3x.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2018-09-05 15:48:39', '2018-09-05 18:48:39', '', 'right', '', 'inherit', 'open', 'closed', '', 'right', '', '', '2018-09-05 15:48:39', '2018-09-05 18:48:39', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/right.png', 0, 'attachment', 'image/png', 0),
(49, 1, '2018-09-05 15:48:42', '2018-09-05 18:48:42', '', 'starativo', '', 'inherit', 'open', 'closed', '', 'starativo', '', '', '2018-09-05 15:48:42', '2018-09-05 18:48:42', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/starativo-1.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2018-09-05 15:48:44', '2018-09-05 18:48:44', '', 'starpass', '', 'inherit', 'open', 'closed', '', 'starpass', '', '', '2018-09-05 15:48:44', '2018-09-05 18:48:44', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/starpass-1.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2018-09-05 15:48:47', '2018-09-05 18:48:47', '', 'tablet', '', 'inherit', 'open', 'closed', '', 'tablet', '', '', '2018-09-05 15:48:47', '2018-09-05 18:48:47', '', 0, 'http://localhost/projetos/sticksports_site/wp-content/uploads/2018/09/tablet.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2018-09-05 16:04:29', '2018-09-05 19:04:29', '', 'One Pager', '', 'publish', 'closed', 'closed', '', 'one-pager', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=52', 1, 'nav_menu_item', '', 0),
(53, 1, '2018-09-05 16:04:30', '2018-09-05 19:04:30', '', 'Solution', '', 'publish', 'closed', 'closed', '', 'solution', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=53', 2, 'nav_menu_item', '', 0),
(54, 1, '2018-09-05 16:04:30', '2018-09-05 19:04:30', '', 'Roadmap', '', 'publish', 'closed', 'closed', '', 'roadmap', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=54', 3, 'nav_menu_item', '', 0),
(55, 1, '2018-09-05 16:04:30', '2018-09-05 19:04:30', '', 'Team', '', 'publish', 'closed', 'closed', '', 'team', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=55', 4, 'nav_menu_item', '', 0),
(56, 1, '2018-09-05 16:04:30', '2018-09-05 19:04:30', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=56', 5, 'nav_menu_item', '', 0),
(57, 1, '2018-09-05 16:04:30', '2018-09-05 19:04:30', '', 'Faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2018-09-24 15:24:37', '2018-09-24 18:24:37', '', 0, 'http://localhost/projetos/sticksports_site/?p=57', 6, 'nav_menu_item', '', 0),
(58, 1, '2018-09-05 16:35:22', '2018-09-05 19:35:22', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.', 'Paul Colins', '', 'publish', 'closed', 'closed', '', 'paul-colins', '', '', '2018-09-05 16:36:24', '2018-09-05 19:36:24', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=58', 0, 'team', '', 0),
(59, 1, '2018-09-05 16:35:56', '2018-09-05 19:35:56', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.', 'Colins', '', 'publish', 'closed', 'closed', '', 'colins', '', '', '2018-09-05 16:36:09', '2018-09-05 19:36:09', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=59', 0, 'team', '', 0),
(60, 1, '2018-09-05 16:36:58', '2018-09-05 19:36:58', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.2', 'Paul Colins 2', '', 'publish', 'closed', 'closed', '', 'paul-colins-2', '', '', '2018-09-05 16:38:22', '2018-09-05 19:38:22', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=60', 0, 'team', '', 0),
(61, 1, '2018-09-05 16:38:24', '2018-09-05 19:38:24', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.2', 'Paul Colins 3', '', 'publish', 'closed', 'closed', '', 'paul-colins-3', '', '', '2018-09-05 16:38:24', '2018-09-05 19:38:24', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=61', 0, 'team', '', 0),
(62, 1, '2018-09-05 16:38:28', '2018-09-05 19:38:28', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.2', 'Paul Colins 4', '', 'publish', 'closed', 'closed', '', 'paul-colins-4', '', '', '2018-09-05 16:38:28', '2018-09-05 19:38:28', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=62', 0, 'team', '', 0),
(63, 1, '2018-09-05 16:38:30', '2018-09-05 19:38:30', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.2', 'Paul Colins 5', '', 'publish', 'closed', 'closed', '', 'paul-colins-5', '', '', '2018-09-05 16:38:30', '2018-09-05 19:38:30', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=63', 0, 'team', '', 0),
(64, 1, '2018-09-05 16:38:56', '2018-09-05 19:38:56', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company', 'Paul Colins 6', '', 'publish', 'closed', 'closed', '', 'paul-colins-6', '', '', '2018-09-06 18:00:34', '2018-09-06 21:00:34', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=team&#038;p=64', 0, 'team', '', 0),
(65, 1, '2018-09-05 16:38:04', '2018-09-05 19:38:04', 'With a background in commercial management, Paul has more than 15 years of experience in the games industry. He founded his first games company in 2004, an Advergaming agency that designed and launched more than 50 web games and campaigns for brands and media platforms. He went on to launch Stick Sports in 2006 with the aim of building a global niche in casual sports games. With a deep understanding of the design process, marketing and finance, Paul is driving the company strategy and innovation. His biggest achievements are recognising early the potential of mobile and he is a pioneer in free-2-play game mechanics.2', 'Paul Colins 2', '', 'inherit', 'closed', 'closed', '', '60-autosave-v1', '', '', '2018-09-05 16:38:04', '2018-09-05 19:38:04', '', 60, 'http://localhost/projetos/sticksports_site/60-autosave-v1/', 0, 'revision', '', 0),
(66, 1, '2018-09-05 17:15:19', '2018-09-05 20:15:19', 'What players win or buy in the game they own outright. The digital assets they create retain value inside or outside of the game. They choose what happens with their assets. We’re bringing blockchain to gamers, not gamers to blockchain.', 'REAL WORD VALUE', '', 'publish', 'closed', 'closed', '', 'real-word-value', '', '', '2018-09-05 17:15:19', '2018-09-05 20:15:19', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=solutions&#038;p=66', 0, 'solutions', '', 0),
(67, 1, '2018-09-05 17:16:12', '2018-09-05 20:16:12', 'What players win or buy in the game they own outright. The digital assets they create retain value inside or outside of the game. They choose what happens with their assets. We’re bringing blockchain to gamers, not gamers to blockchain.', 'REAL WORD VALUE', '', 'publish', 'closed', 'closed', '', 'real-word-value-2', '', '', '2018-09-05 17:16:26', '2018-09-05 20:16:26', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=solutions&#038;p=67', 0, 'solutions', '', 0),
(68, 1, '2018-09-05 17:17:07', '2018-09-05 20:17:07', 'What players win or buy in the game they own outright. The digital assets they create retain value inside or outside of the game. They choose what happens with their assets. We’re bringing blockchain to gamers, not gamers to blockchain.', 'REAL WORD VALUE', '', 'publish', 'closed', 'closed', '', 'real-word-value-3', '', '', '2018-09-05 17:17:20', '2018-09-05 20:17:20', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=solutions&#038;p=68', 0, 'solutions', '', 0),
(72, 1, '2018-09-17 12:30:16', '2018-09-17 15:30:16', '', '', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2018-09-17 12:30:16', '2018-09-17 15:30:16', '', 71, 'http://localhost/projetos/sticksports_site/71-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `ss_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(73, 1, '2018-09-17 12:30:29', '2018-09-17 15:30:29', '', 'Faq', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2018-09-17 12:30:29', '2018-09-17 15:30:29', '', 71, 'http://localhost/projetos/sticksports_site/71-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2018-09-17 16:22:00', '2018-09-17 19:22:00', 'We are working hard to launch the game in April 2019 to coincide with the start of the Indian Premier League (IPL) season.', 'When can I expect to see a working version of the game?', '', 'publish', 'closed', 'closed', '', 'when-can-i-expect-to-see-a-working-version-of-the-game', '', '', '2018-09-17 16:22:00', '2018-09-17 19:22:00', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=74', 0, 'questions', '', 0),
(75, 1, '2018-09-17 16:22:26', '2018-09-17 19:22:26', 'Traditionally, achievements, rewards, accolades and assets have been earned in mobile games through time, skill or purchases. These accumulated items were then licenced to the player by the publisher. Differing from a licence, ownership of digital assets can only be assured through an ability to freely trade the items with others without the need to refer to a controlling authority. Being able to own, trade, loan, customise and develop the key components of a game is a radical evolution in our industry.', 'What problem or need in the market is Stick Sports solving?', '', 'publish', 'closed', 'closed', '', 'what-problem-or-need-in-the-market-is-stick-sports-solving', '', '', '2018-09-17 16:22:26', '2018-09-17 19:22:26', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=75', 0, 'questions', '', 0),
(76, 1, '2018-09-17 16:22:42', '2018-09-17 19:22:42', 'The issue of true digital ownership has not been solved because until very recently the technology did not exist to address the problem. Blockchain enables the ownership, trading and loaning of digital assets. We believe this will cause a shift in the games industry - transitioning games from content to be consumed to a hobby to participate in.', 'Why has this problem not been solved in the past?', '', 'publish', 'closed', 'closed', '', 'why-has-this-problem-not-been-solved-in-the-past', '', '', '2018-09-17 16:22:42', '2018-09-17 19:22:42', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=76', 0, 'questions', '', 0),
(77, 1, '2018-09-17 16:23:28', '2018-09-17 19:23:28', 'Stick Sports\' new game will be available globally and free to download from the Apple App Store and Google Play.', 'What country will this game be available in?', '', 'publish', 'closed', 'closed', '', 'what-country-will-this-game-be-available-in', '', '', '2018-09-17 16:23:28', '2018-09-17 19:23:28', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=77', 0, 'questions', '', 0),
(78, 1, '2018-09-17 16:23:39', '2018-09-17 19:23:39', 'Like all mobile games publishers, Stick Sports generates revenue through in-app purchases (IAP) and advertising. We anticipate that the popularity of our new game will result in an increase in daily active users (DAUs) and therefore IAPs. Further, Stick Sports also plan to mint and sell exciting and unique items that can be used within the game - an avatar of your favourite player, for example. Stick Sports will receive a portion of these initial sales to help operate the platform, develop new content and ensure the longevity of the ecosystem.', 'How does Stick Sports make profit?', '', 'publish', 'closed', 'closed', '', 'how-does-stick-sports-make-profit', '', '', '2018-09-17 16:23:39', '2018-09-17 19:23:39', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=78', 0, 'questions', '', 0),
(79, 1, '2018-09-17 16:23:48', '2018-09-17 19:23:48', 'Stick Sports currently have a range of sport-based mobile games available to download on iOS and Android. Our Alpha version of Stick Sports\' first blockchain-enabled game will be delivered in September 2018. Please join our Telegram group for updates and announcements.', 'Do you have an Minimum Viable Product?', '', 'publish', 'closed', 'closed', '', 'do-you-have-an-minimum-viable-product', '', '', '2018-09-17 16:23:48', '2018-09-17 19:23:48', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=79', 0, 'questions', '', 0),
(80, 1, '2018-09-17 16:24:06', '2018-09-17 19:24:06', 'Stick Sports has two main types of competitor: free-to-play sports mobile games publishers who may also be interested in integrating blockchain into to their games; and crypto-games startups who may decide to create a sport related game. Addressing the first, Stick Sports is well positioned as a first-mover transitioning from the traditional free-to-play format to a new blockchain-enabled model. Second, we believe our proven track record as a publisher and our passionate user base sets us apart from many crypto games that often have limited DAUs and inexperinced game deisgn teams. We are confident that Stick Sports maintains a competitive advantage in the industry as one of the leading mobile sports based games publishers - with over 70 million downloads across iOS and Android. Stick Cricket is the highest rated cricket game on iOS and Android and Stick Cricket Super League has achieved over 100k downloads on launch day alone.', 'What is your competition?', '', 'publish', 'closed', 'closed', '', 'what-is-your-competition', '', '', '2018-09-17 16:24:06', '2018-09-17 19:24:06', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=80', 0, 'questions', '', 0),
(81, 1, '2018-09-17 16:24:25', '2018-09-17 19:24:25', 'The key differentiation of Stick Sports compared to other blockchain games is the strong brand, depth of expertise, loyal following and industry network it has achieved through its mobile sports games. Stick Sports will successfully leverage all of these factors in order to gain a significant competitive edge over other crypto-games in the market.', 'How does your product differentiate from the other blockchain games?', '', 'publish', 'closed', 'closed', '', 'how-does-your-product-differentiate-from-the-other-blockchain-games', '', '', '2018-09-17 16:24:25', '2018-09-17 19:24:25', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=81', 0, 'questions', '', 0),
(82, 1, '2018-09-17 16:24:53', '2018-09-17 19:24:53', 'Stick Sports has over a decade of provenance in the mobile games space. We have built, launched and maintained a wide range of sports titles that have achieved over 70 million downloads and currently maintain 450k DAUs. With this background, we are confident our next cricket game will continue this track record of success. We intend to integrate established and trusted blockchain technology (Ethereum and the ERC-20 and ERC-721 standards) to ensure the innovation we are introducing successfully delivers digital ownership of assets to our players.', 'Why do you think your team can build this game?', '', 'publish', 'closed', 'closed', '', 'why-do-you-think-your-team-can-build-this-game', '', '', '2018-09-17 16:24:53', '2018-09-17 19:24:53', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=82', 0, 'questions', '', 0),
(83, 1, '2018-09-17 16:25:14', '2018-09-17 19:25:14', 'Our first title, Stick Cricket, was launched 14 years ago as an online free-to-play game in 2004.', 'How long has Stick Sports been in business?', '', 'publish', 'closed', 'closed', '', 'how-long-has-stick-sports-been-in-business', '', '', '2018-09-17 16:25:14', '2018-09-17 19:25:14', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=83', 0, 'questions', '', 0),
(84, 1, '2018-09-17 16:25:26', '2018-09-17 19:25:26', 'Stick Cricket was created in 2004 by Colin (Founder) and Paul (CEO) who have been working together ever since. Stick Sports has a core team of 14 members experienced games professionals and leaders in the free-to-play mobile games industry.', 'How long has the team been working together?', '', 'publish', 'closed', 'closed', '', 'how-long-has-the-team-been-working-together', '', '', '2018-09-17 16:25:26', '2018-09-17 19:25:26', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=84', 0, 'questions', '', 0),
(85, 1, '2018-09-17 16:25:40', '2018-09-17 19:25:40', 'samantha.sanchis@sticksports.com jono.russel@sticksports.com', 'Contact Email', '', 'publish', 'closed', 'closed', '', 'contact-email', '', '', '2018-09-17 16:25:40', '2018-09-17 19:25:40', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=85', 0, 'questions', '', 0),
(86, 1, '2018-09-17 16:25:51', '2018-09-17 19:25:51', 'A blockchain is a digitised, decentralised, public ledger of transactions. Blocks of information are added to a continuously growing chain in chronological order, allowing participants to autonomously store and verify transactions without the need of a central authority or record-keeper.', 'What is Blockchain?', '', 'publish', 'closed', 'closed', '', 'what-is-blockchain', '', '', '2018-09-17 16:25:51', '2018-09-17 19:25:51', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=86', 0, 'questions', '', 0),
(87, 1, '2018-09-17 16:26:01', '2018-09-17 19:26:01', 'Ethereum is a blockchain-based decentralised platform on which decentralised applications can be run. Ethereum is considered a world computer, whereby computer code is executed simultaneously across the entire network and the result of that computation can be verified by anyone. This platform is where many “decentralised applications” or Dapps are being built today.', 'What is Ethereum?', '', 'publish', 'closed', 'closed', '', 'what-is-ethereum', '', '', '2018-09-17 16:26:01', '2018-09-17 19:26:01', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=87', 0, 'questions', '', 0),
(88, 1, '2018-09-17 16:26:43', '2018-09-17 19:26:43', 'Decentralised applications (Dapps) are applications that run on many computers simultaneously as opposed to a single computer. Every computer running the application must agree on the result or it is considered invalid. Because of this, a single authority does not deploy or control them once they are deployed on the network. Smart Contracts connect Dapps to the blockchain.', 'What are Dapps?', '', 'publish', 'closed', 'closed', '', 'what-are-dapps', '', '', '2018-09-17 16:26:43', '2018-09-17 19:26:43', '', 0, 'http://localhost/projetos/sticksports_site/?post_type=questions&#038;p=88', 0, 'questions', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_termmeta`
--

DROP TABLE IF EXISTS `ss_termmeta`;
CREATE TABLE IF NOT EXISTS `ss_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_terms`
--

DROP TABLE IF EXISTS `ss_terms`;
CREATE TABLE IF NOT EXISTS `ss_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_terms`
--

INSERT INTO `ss_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Stick Sports', 'stick-sports', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_term_relationships`
--

DROP TABLE IF EXISTS `ss_term_relationships`;
CREATE TABLE IF NOT EXISTS `ss_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_term_relationships`
--

INSERT INTO `ss_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(5, 1, 0),
(52, 2, 0),
(53, 2, 0),
(54, 2, 0),
(55, 2, 0),
(56, 2, 0),
(57, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_term_taxonomy`
--

DROP TABLE IF EXISTS `ss_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `ss_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_term_taxonomy`
--

INSERT INTO `ss_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_usermeta`
--

DROP TABLE IF EXISTS `ss_usermeta`;
CREATE TABLE IF NOT EXISTS `ss_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_usermeta`
--

INSERT INTO `ss_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'sticksports'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'ss_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'ss_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"eef7677655d2f33a83ff8488e9981bf06db3e5d24e599d0a98264b1eed7feadb\";a:4:{s:10:\"expiration\";i:1537972177;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1537799377;}s:64:\"1d9f873cffcef85f0b29bed48b791e272249e629f808e25cd2617909cd40dfb2\";a:4:{s:10:\"expiration\";i:1537985857;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1537813057;}s:64:\"62a39a77c7dcf131728945a554159a83072415628f855a10219ecb79fd0f95ac\";a:4:{s:10:\"expiration\";i:1538063217;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1537890417;}}'),
(17, 1, 'ss_dashboard_quick_press_last_post_id', '89'),
(21, 1, 'ss_yoast_notifications', 'a:3:{i:0;a:2:{s:7:\"message\";s:1139:\"We\'ve noticed you\'ve been using Yoast SEO for some time now; we hope you love it! We\'d be thrilled if you could <a href=\"https://yoa.st/rate-yoast-seo?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.1.1&role=administrator&days_active=21\">give us a 5 stars rating on WordPress.org</a>!\n\nIf you are experiencing issues, <a href=\"https://yoa.st/bugreport?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.1.1&role=administrator&days_active=21\">please file a bug report</a> and we\'ll do our best to help you out.\n\nBy the way, did you know we also have a <a href=\'https://yoa.st/premium-notification?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.1.1&role=administrator&days_active=21\'>Premium plugin</a>? It offers advanced features, like a redirect manager and support for multiple keywords. It also comes with 24/7 personal support.\n\n<a class=\"button\" href=\"http://localhost/projetos/sticksports_site/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Please don\'t show me this notification anymore</a>\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.80000000000000004;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:185:\"Don\'t miss your crawl errors: <a href=\"http://localhost/projetos/sticksports_site/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:2;a:2:{s:7:\"message\";s:239:\"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href=\"http://localhost/projetos/sticksports_site/wp-admin/options-reading.php\">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.\";s:7:\"options\";a:9:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(19, 1, 'ss_user-settings', 'mfold=o&libraryContent=browse'),
(20, 1, 'ss_user-settings-time', '1536173510'),
(22, 1, 'last_login_time', '2018-09-25 12:46:57'),
(23, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(24, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:24:\"wpseo-dashboard-overview\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(25, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(26, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:18:\"add-post-type-team\";i:1;s:23:\"add-post-type-solutions\";i:2;s:12:\"add-post_tag\";i:3;s:21:\"add-categoriaDestaque\";}'),
(27, 1, 'nav_menu_recently_edited', '2'),
(28, 1, 'closedpostboxes_team', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(29, 1, 'metaboxhidden_team', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(30, 1, 'meta-box-order_team', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:37:\"detailsMetaboxTeam,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(31, 1, 'screen_layout_team', '2'),
(32, 1, 'closedpostboxes_solutions', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(33, 1, 'metaboxhidden_solutions', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_users`
--

DROP TABLE IF EXISTS `ss_users`;
CREATE TABLE IF NOT EXISTS `ss_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_users`
--

INSERT INTO `ss_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'sticksports', '$P$BcMXgkkdksM9ZUku9tLMZDM.A0MZci0', 'sticksports', 'contato@hcdesenvolvimentos.com.br', '', '2018-09-04 17:15:28', '', 0, 'sticksports');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_campaign`
--

DROP TABLE IF EXISTS `ss_wysija_campaign`;
CREATE TABLE IF NOT EXISTS `ss_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_campaign`
--

INSERT INTO `ss_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, '5 Minute User Guide', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_campaign_list`
--

DROP TABLE IF EXISTS `ss_wysija_campaign_list`;
CREATE TABLE IF NOT EXISTS `ss_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text,
  PRIMARY KEY (`list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_custom_field`
--

DROP TABLE IF EXISTS `ss_wysija_custom_field`;
CREATE TABLE IF NOT EXISTS `ss_wysija_custom_field` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_email`
--

DROP TABLE IF EXISTS `ss_wysija_email`;
CREATE TABLE IF NOT EXISTS `ss_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext,
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_email`
--

INSERT INTO `ss_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, '5 Minute User Guide', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>5 Minute User Guide</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Display problems? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">View this newsletter in your browser.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"http://localhost/projetos/sticksports_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Step 1:</strong> hey, click on this text!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">To edit, simply click on this block of text.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Step 2:</strong> play with this image</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"http://localhost/projetos/sticksports_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Position your mouse over the image to the left.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Step 3:</strong> drop content here</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Drag and drop <strong>text, posts, dividers.</strong> Look on the right!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">You can even <strong>social bookmarks</strong> like these:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/sticksports_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Step 4:</strong> and the footer?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Change the footer\'s content in MailPoet\'s <strong>Settings</strong> page.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"http://localhost/projetos/sticksports_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Unsubscribe</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1536085144, 1536085144, NULL, 'info@localhost', 'sticksports', 'info@localhost', 'sticksports', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyNToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9zdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6OToidGh1bWJfdXJsIjtzOjEzMzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9zdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi0xNTB4MTUwLnBuZyI7fX19', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMi45IjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMjU6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3Mvc3RpY2tzcG9ydHNfc2l0ZS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9oZWFkZXIucG5nIjtzOjU6IndpZHRoIjtpOjYwMDtzOjY6ImhlaWdodCI7aTo3MjtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7fXM6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDtzOjQ6InR5cGUiO3M6NjoiaGVhZGVyIjt9czo0OiJib2R5IjthOjk6e3M6NzoiYmxvY2stMSI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czoxNDg6IlBHZ3lQanh6ZEhKdmJtYytVM1JsY0NBeE9qd3ZjM1J5YjI1blBpQm9aWGtzSUdOc2FXTnJJRzl1SUhSb2FYTWdkR1Y0ZENFOEwyZ3lQanh3UGxSdklHVmthWFFzSUhOcGJYQnNlU0JqYkdsamF5QnZiaUIwYUdseklHSnNiMk5ySUc5bUlIUmxlSFF1UEM5d1BnPT0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aToxO3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay0yIjthOjU6e3M6ODoicG9zaXRpb24iO2k6MjtzOjQ6InR5cGUiO3M6NzoiZGl2aWRlciI7czozOiJzcmMiO3M6ODc6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3Mvc3RpY2tzcG9ydHNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay0zIjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjcyOiJQR2d5UGp4emRISnZibWMrVTNSbGNDQXlPand2YzNSeWIyNW5QaUJ3YkdGNUlIZHBkR2dnZEdocGN5QnBiV0ZuWlR3dmFESSsiO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTozO3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay00IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjcyOiJQSEErVUc5emFYUnBiMjRnZVc5MWNpQnRiM1Z6WlNCdmRtVnlJSFJvWlNCcGJXRm5aU0IwYnlCMGFHVWdiR1ZtZEM0OEwzQSsiO31zOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyNToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9zdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6NToid2lkdGgiO2k6MjgxO3M6NjoiaGVpZ2h0IjtpOjE5MDtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NDtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNSI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjU7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjg3OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3N0aWNrc3BvcnRzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9kaXZpZGVycy9zb2xpZC5qcGciO3M6NToid2lkdGgiO2k6NTY0O3M6NjoiaGVpZ2h0IjtpOjE7fXM6NzoiYmxvY2stNiI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czoyNjA6IlBHZ3lQanh6ZEhKdmJtYytVM1JsY0NBek9qd3ZjM1J5YjI1blBpQmtjbTl3SUdOdmJuUmxiblFnYUdWeVpUd3ZhREkrUEhBK1JISmhaeUJoYm1RZ1pISnZjQ0E4YzNSeWIyNW5QblJsZUhRc0lIQnZjM1J6TENCa2FYWnBaR1Z5Y3k0OEwzTjBjbTl1Wno0Z1RHOXZheUJ2YmlCMGFHVWdjbWxuYUhRaFBDOXdQanh3UGxsdmRTQmpZVzRnWlhabGJpQThjM1J5YjI1blBuTnZZMmxoYkNCaWIyOXJiV0Z5YTNNOEwzTjBjbTl1Wno0Z2JHbHJaU0IwYUdWelpUbzhMM0ErIjt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NjtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNyI7YTo1OntzOjU6IndpZHRoIjtpOjE4NDtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6NToiaXRlbXMiO2E6Mzp7aTowO2E6Nzp7czozOiJ1cmwiO3M6Mzg6Imh0dHA6Ly93d3cuZmFjZWJvb2suY29tL21haWxwb2V0cGx1Z2luIjtzOjM6ImFsdCI7czo4OiJGYWNlYm9vayI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czoxMDE6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3Mvc3RpY2tzcG9ydHNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZmFjZWJvb2sucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjE7YTo3OntzOjM6InVybCI7czozMjoiaHR0cDovL3d3dy50d2l0dGVyLmNvbS9tYWlsX3BvZXQiO3M6MzoiYWx0IjtzOjc6IlR3aXR0ZXIiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6MTAwOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3N0aWNrc3BvcnRzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL3R3aXR0ZXIucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjI7YTo3OntzOjM6InVybCI7czozMzoiaHR0cHM6Ly9wbHVzLmdvb2dsZS5jb20vK01haWxwb2V0IjtzOjM6ImFsdCI7czo2OiJHb29nbGUiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6OTk6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3Mvc3RpY2tzcG9ydHNfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjg3OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3N0aWNrc3BvcnRzX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9kaXZpZGVycy9zb2xpZC5qcGciO3M6NToid2lkdGgiO2k6NTY0O3M6NjoiaGVpZ2h0IjtpOjE7fXM6NzoiYmxvY2stOSI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czoxNzI6IlBHZ3lQanh6ZEhKdmJtYytVM1JsY0NBME9qd3ZjM1J5YjI1blBpQmhibVFnZEdobElHWnZiM1JsY2o4OEwyZ3lQanh3UGtOb1lXNW5aU0IwYUdVZ1ptOXZkR1Z5SjNNZ1kyOXVkR1Z1ZENCcGJpQk5ZV2xzVUc5bGRDZHpJRHh6ZEhKdmJtYytVMlYwZEdsdVozTThMM04wY205dVp6NGdjR0ZuWlM0OEwzQSsiO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo5O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9fXM6NjoiZm9vdGVyIjthOjU6e3M6NDoidGV4dCI7TjtzOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyNToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9zdGlja3Nwb3J0c19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2Zvb3Rlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjQ2O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJmb290ZXIiO319', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirm your subscription to Stick Sports', 'Hello!\n\nHurray! You\'ve subscribed to our site.\nWe need you to activate your subscription to the list(s): [lists_to_confirm] by clicking the link below: \n\n[activation_link]Click here to confirm your subscription.[/activation_link]\n\nThank you,\n\n The team!\n', 1536085145, 1536085145, NULL, 'info@localhost', 'sticksports', 'info@localhost', 'sticksports', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_email_user_stat`
--

DROP TABLE IF EXISTS `ss_wysija_email_user_stat`;
CREATE TABLE IF NOT EXISTS `ss_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_email_user_url`
--

DROP TABLE IF EXISTS `ss_wysija_email_user_url`;
CREATE TABLE IF NOT EXISTS `ss_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_form`
--

DROP TABLE IF EXISTS `ss_wysija_form`;
CREATE TABLE IF NOT EXISTS `ss_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_form`
--

INSERT INTO `ss_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Subscribe to our Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjY1OiJDaGVjayB5b3VyIGluYm94IG9yIHNwYW0gZm9sZGVyIG5vdyB0byBjb25maXJtIHlvdXIgc3Vic2NyaXB0aW9uLiI7czo1OiJsaXN0cyI7YToxOntpOjA7czoxOiIxIjt9czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mjp7aTowO2E6NDp7czo0OiJuYW1lIjtzOjU6IkVtYWlsIjtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6NToiZW1haWwiO3M6NjoicGFyYW1zIjthOjI6e3M6NToibGFiZWwiO3M6NToiRW1haWwiO3M6ODoicmVxdWlyZWQiO2I6MTt9fWk6MTthOjQ6e3M6NDoibmFtZSI7czo2OiJTdWJtaXQiO3M6NDoidHlwZSI7czo2OiJzdWJtaXQiO3M6NToiZmllbGQiO3M6Njoic3VibWl0IjtzOjY6InBhcmFtcyI7YToxOntzOjU6ImxhYmVsIjtzOjEwOiJTdWJzY3JpYmUhIjt9fX1zOjc6ImZvcm1faWQiO2k6MTt9', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_list`
--

DROP TABLE IF EXISTS `ss_wysija_list`;
CREATE TABLE IF NOT EXISTS `ss_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_list`
--

INSERT INTO `ss_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'My first list', 'my-first-list', 'The list created automatically on install of the MailPoet.', 0, 0, 1, 1, 1536085144, 0),
(2, 'WordPress Users', 'users', 'The list created automatically on import of the plugin\'s subscribers : \"WordPress', 0, 0, 0, 0, 1536085145, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_queue`
--

DROP TABLE IF EXISTS `ss_wysija_queue`;
CREATE TABLE IF NOT EXISTS `ss_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`),
  KEY `SENT_AT_INDEX` (`send_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_subscriber_ips`
--

DROP TABLE IF EXISTS `ss_wysija_subscriber_ips`;
CREATE TABLE IF NOT EXISTS `ss_wysija_subscriber_ips` (
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`created_at`,`ip`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_url`
--

DROP TABLE IF EXISTS `ss_wysija_url`;
CREATE TABLE IF NOT EXISTS `ss_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_url_mail`
--

DROP TABLE IF EXISTS `ss_wysija_url_mail`;
CREATE TABLE IF NOT EXISTS `ss_wysija_url_mail` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_user`
--

DROP TABLE IF EXISTS `ss_wysija_user`;
CREATE TABLE IF NOT EXISTS `ss_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL_UNIQUE` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_user`
--

INSERT INTO `ss_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'contato@hcdesenvolvimentos.com.br', '', '', '', '0', NULL, NULL, NULL, '', 1536085146, 1, 'hcdesenvolvimentos.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_user_field`
--

DROP TABLE IF EXISTS `ss_wysija_user_field`;
CREATE TABLE IF NOT EXISTS `ss_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_user_field`
--

INSERT INTO `ss_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'First name', 'firstname', 0, NULL, '', 0, 'Please enter first name'),
(2, 'Last name', 'lastname', 0, NULL, '', 0, 'Please enter last name');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_user_history`
--

DROP TABLE IF EXISTS `ss_wysija_user_history`;
CREATE TABLE IF NOT EXISTS `ss_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_wysija_user_list`
--

DROP TABLE IF EXISTS `ss_wysija_user_list`;
CREATE TABLE IF NOT EXISTS `ss_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`list_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ss_wysija_user_list`
--

INSERT INTO `ss_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1536085144, 0),
(2, 1, 1536085145, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_yoast_seo_links`
--

DROP TABLE IF EXISTS `ss_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `ss_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ss_yoast_seo_meta`
--

DROP TABLE IF EXISTS `ss_yoast_seo_meta`;
CREATE TABLE IF NOT EXISTS `ss_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ss_yoast_seo_meta`
--

INSERT INTO `ss_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(12, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(4, 0, 0),
(6, 0, 0),
(7, 0, 0),
(71, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0),
(77, 0, 0),
(78, 0, 0),
(79, 0, 0),
(80, 0, 0),
(81, 0, 0),
(82, 0, 0),
(83, 0, 0),
(84, 0, 0),
(85, 0, 0),
(86, 0, 0),
(87, 0, 0),
(88, 0, 0),
(70, 0, 0),
(69, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
